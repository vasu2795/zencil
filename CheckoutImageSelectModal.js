import React from 'react';
import R from 'ramda';

// Modal to collect a url for the checkout image
export default class CheckoutImageSelectModal extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      modalInstance: null,
      imageUrl: null,
      imageLoaded: null
    };
  }

  componentDidMount() {
    this.state = {
      imageUrl: null,
      imageLoaded: null,
      modalInstance: $('#image-select-modal').modal()
    };

    this.setState(this.state);
  }

  show() {
    this.state.modalInstance.modal('show');
  }

  hide() {
    this.state.modalInstance.modal('hide');
  }

  loadImage(ev) {
    ev.nativeEvent.preventDefault();
    this.refs['logoImage'].src = this.state.imageUrl;
    this.state.imageLoaded = 'loading';
    this.setState(this.state);
  }

  onImageSelected() {
    this.props.onImageSelected(this.state.imageUrl);
  }

  render() {
    let bindData = (ev) => {
      // Clear the message only if error is shown.
      if(this.state.imageLoaded == 'error') {
        this.state.imageLoaded = null;
      }

      this.state.imageUrl = ev.nativeEvent.target.value;
      this.setState(this.state);
    };

    let onImageLoad = () => {
      this.state.imageLoaded = 'loaded';
      this.setState(this.state);
    };

    let onImageError = () => {
      this.state.imageLoaded = 'error';
      this.setState(this.state);
    };

    return (
      <div className="ui small modal" id="image-select-modal">
        <div className="header">Checkout title logo</div>
        <div className="content">
          <div
              className={this.state.imageLoaded == 'loading' ? 'ui loading segment' : 'ui segment'}
              style={{
                display: ((this.state.imageLoaded == 'error' || this.state.imageLoaded == null) ? 'none' : 'block')
              }}
          >
            <img
                onLoad={onImageLoad}
                onError={onImageError}
                ref="logoImage"
                className="image"/>
          </div>
          <div className="description">
            {do {
               if(this.state.imageLoaded == 'error') {
                 <div className="ui error message">
                   <div className="header">Failed to load image!</div>
                   Sorry, we could not find any image at <a href={this.state.imageUrl}>{this.state.imageUrl}</a>.
                   Would you like to try again?
                 </div>
               } else if(this.state.imageLoaded == null) {
                 <div className="ui info message">
                   <div className="header">Enter an image URL</div>
                   Enter an image URL and we will load it here for you.
                 </div>
               }
             }}
            <form className="ui basic form" onSubmit={::this.loadImage}>
              <div className="field">
                <div className="ui action input">
                  <input required="true" value={this.state.imageUrl} onChange={bindData} />
                  {do {
                     if(this.state.imageLoaded == 'loaded') {
                       <button onClick={::this.onImageSelected}
                               className="ui green button"
                               type="button">Use this image.</button>
                     } else if (this.state.imageLoaded == 'loading'){
                       <button className="ui disabled loading button"
                               type="button">Loading</button>
                     } else if(this.state.imageLoaded == 'error') {
                       <button className="ui red button"
                               type="button">Failed</button>
                     } else {
                       <button className="ui blue button"
                               type="submit">Load</button>
                     }
                   }}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

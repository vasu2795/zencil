defmodule Zencil.AuthController do
  use Zencil.Web, :controller
  import Ecto.Query
  import Ecto.Changeset
  alias Zencil.{Repo, User}

  def request(conn, _params) do
    render(conn, "request.html", callback_url: Helpers.callback_url(conn))
  end

  def generate_password_hash(email_id, password) do
     :crypto.hash(:sha256, "#{password}{#{email_id}}") |> Base.encode64
  end

#   def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _params) do
#     IO.inspect "here...."
#   case UserFromAuth.find_or_create(auth) do
#     {:ok, user} ->
#       IO.inspect "user"
#       IO.inspect user
#       conn
#       |> put_flash(:info, "Successfully authenticated.")
#       |> put_session(:current_user, user)
#       |> redirect(to: "/")
#     {:error, reason} ->
#       IO.inspect "reason"
#       IO.inspect reason
#       conn
#       |> put_flash(:error, reason)
#       |> redirect(to: "/")
#   end
# end
#
# def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
#   IO.inspect "failure"
#   IO.inspect _params
#   conn
#   |> put_flash(:error, "Failed to authenticate.")
#   |> redirect(to: "/")
# end
#
  def login(conn, %{"password" => password,"email_id" => email_id}) do
      IO.inspect "email id check"
      IO.inspect email_id
      case User.authenticate_user(email_id, password) do
        {:ok, user} ->
          user = Map.put(user, "status", :logged_in)
          IO.inspect "logged in"
          conn
            |> put_status(200)
            |> json(user)

        "invalid_creds" ->
          conn
            |> put_status(400)
            |> json(%{"status": "invalid_creds"})
      end
  end

  def google_authenticate(conn,%{"email_id" => email_id, "id" => id,"name" => name, "image_url" => image_url}) do
    case User.google_authenticate(email_id,id,name,image_url) do
      user -> IO.inspect "returning point"
        IO.inspect user
        conn |> put_status(200) |> json(user)
      _-> conn |> put_status(400) |> json(%{"status": "failure"})
    end
  end

  def facebook_authenticate(conn,%{"email_id"=> email,"user_id" => id, "name" => name, "image_url" => image_url}) do
    IO.inspect "Facebook authneticating"
    IO.inspect User.facebook_authenticate(email,id,name,image_url)
    case User.facebook_authenticate(email,id,name,image_url) do
      user -> IO.inspect "returning point"
        IO.inspect user
        conn |> put_status(200) |> json(user)
      _-> conn |> put_status(400) |> json(%{"status": "failure"})
    end
  end

  def sign_up(conn, %{"occupation"=> occupation,"age" => age, "email_id" => email_id, "location" => location,"name" => name,"password" => password}) do
    IO.inspect email_id
    user = User |> where(email_id: ^email_id) |> Repo.one
    if user do
      IO.inspect "already exist"
      conn
        |> put_status(201) |> json (%{"status": "already_exists"})
    else
      user_data = %{"occupation" => occupation,"age" => age, "email_id" => email_id, "location" => location,"username" => name,"password" => generate_password_hash(email_id,password)}
      IO.inspect user_data
      user_data = user_data |> Enum.filter(fn {k, v} -> v end) |> Enum.into(%{})
      IO.inspect "user data is "
      IO.inspect user_data
      coloumns = user_data |> Map.keys |>  Enum.map(fn(x) -> String.to_atom(x) end)
      IO.inspect coloumns
      user = %User{}
                |> cast(user_data,coloumns)
                |> validate_required(coloumns)
                |> Repo.insert!
      IO.inspect user
      if user do
         conn |> put_status(200) |> json(user)
      else
         conn |> put_status(400) |> json(%{"status": "failed"})
      end
    end
  end

end

defmodule Zencil.PageController do
  use Zencil.Web, :controller

  def ping(conn, params) do
    name = Node.self
    json conn, %{"name": name}
  end

  def index(conn, _params) do
    render conn, "index.html"
  end

end

defmodule Zencil.PostController do
  use Zencil.Web, :controller
  import Plug.Conn
  import Ecto.Query
  import Ecto.Changeset

  alias Zencil.{User,PostLog}
  def list_posts(conn, %{"category"=> category_id,"sortBy" => sortBy,"status" => status}) do
    IO.inspect "params in elixir"
    category = Zencil.Category
                    |> where(id: ^category_id)
                    |> Repo.one
                    |> Map.get(:category)
    IO.inspect category
    posts = case sortBy  do


              "Latest" ->         Zencil.Post
                                  |> Zencil.Post.sorted(String.to_atom("date_created"))
                                  # |> where(category: ^category)

                                  |> Repo.all
                                  |> Enum.reverse

              "A-Z" ->     Zencil.Post
                                  |> Zencil.Post.sorted(String.to_atom("title"))
                                  # |> where(category: ^category)
                                  |> Repo.all
                                  |> Enum.reverse

                _->          Zencil.Post
                                    # |> where(category: ^category)
                                    |> where(status: ^status)
                                    |> Zencil.Post.sorted(String.to_atom("date_created"))
                                    |> Repo.all
                                    |> Enum.reverse
            end

      render conn , posts: posts
  end

  def list_pending_posts(conn, _params) do
    status = "pending"
    posts = Zencil.Post
                |> where(status: ^status)
                |> Zencil.Post.sorted(String.to_atom(_params["_sort"]))
                |> Repo.all
                |> Enum.map(fn x -> x end)
    posts = case _params["_order"] do
              "DESC" -> Enum.reverse(posts)
              _-> posts
            end
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count",to_string(length(posts)))
    render conn , posts: posts
  end

  def like_post(conn,%{"post_id" => post_id,"profile_id" => profile_id}) do
    IO.inspect "reached like post"
    IO.inspect post_id
    data = PostLog |> where(post_id: ^post_id) |> where(client_id: ^profile_id) |> Repo.one
    IO.inspect data
    if data do
      liked = data |> cast(%{"liked" => true},[:liked]) |> Repo.update!
    else
      liked = %PostLog{} |> cast(%{"post_id" => post_id,"client_id" => profile_id,"liked" => true,"view" => false},[:post_id,:client_id,:view,:liked])
              |> validate_required([:post_id,:client_id,:liked,:view]) |> Repo.insert!
    end
    json conn,liked
  end

  def unlike_post(conn,%{"post_id" => post_id,"profile_id" => profile_id}) do
    IO.inspect "reached like post"
    IO.inspect post_id
    data = PostLog |> where(post_id: ^post_id) |> where(client_id: ^profile_id) |> Repo.one
    liked = data |> cast(%{"liked" => false},[:liked]) |> Repo.update!
    json conn,liked
  end


  def list_deleted_posts(conn, _params) do
    status = "deleted"
    posts = Zencil.Post
                |> where(status: ^status)
                |> Zencil.Post.sorted(String.to_atom(_params["_sort"]))
                |> Repo.all
                |> Enum.map(fn x -> x end)
    posts = case _params["_order"] do
              "DESC" -> Enum.reverse(posts)
              _-> posts
            end
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count",to_string(length(posts)))
    render conn , posts: posts
  end

  def list_discarded_posts(conn, _params) do
    status = "discarded"
    posts = Zencil.Post
                |> where(status: ^status)
                |> Zencil.Post.sorted(String.to_atom(_params["_sort"]))
                |> Repo.all
                |> Enum.map(fn x -> x end)
    posts = case _params["_order"] do
              "DESC" -> Enum.reverse(posts)
              _-> posts
            end
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count",to_string(length(posts)))
    render conn , posts: posts
  end

  def load_article(conn,%{"id" => id,"profile_id" => profile_id}) do
    post = Zencil.Post
                |> where(id: ^id)
                |> Repo.one
                |> Repo.preload(:user)

    IO.inspect "posts is "
    user = User |> where(id: ^post.user_id) |> Repo.one
    IO.inspect post
    if (Repo.preload(user,:followers)) do
                  IO.inspect "here....."
                  followers =(Repo.preload(user,:followers)).followers |> Enum.map(fn x ->  User |> where(id: ^(Map.get(x,:follower))) |>Repo.one end)
                  IO.inspect "followers "
                  IO.inspect followers
    else
        followers = []
    end

    entry = Zencil.PostLog |> where(post_id: ^id) |> where(client_id: ^profile_id) |> Repo.one
    unless entry do
       data = %Zencil.PostLog{} |> cast(%{"post_id" => id,"client_id" => profile_id,"liked" => false,"view" => true},[:post_id,:client_id,:view,:liked])
                |> validate_required([:view,:liked,:client_id,:post_id]) |> Repo.insert!
    end

    viewed = Repo.preload(post,:post_log).post_log |> Enum.map(fn x -> Map.get(x,:client_id) end )
    liked = Repo.preload(post,:post_log).post_log |> Enum.filter(fn x -> Map.get(x,:liked) == true end ) |> Enum.map(fn x -> Map.get(x,:client_id) end)
    IO.inspect "viewer and liker data"
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    conn |> put_status(200) |> json(%{"post": post, "user": post.user, "followers": followers,"views": viewed,"likes": liked})

  end

  def get_post(conn, %{"id" => id}) do
    post = Zencil.Post
                |> where(id: ^id)
                |> Repo.one
                |> Repo.preload(:user)

    IO.inspect "posts is "
    user = User |> where(id: ^post.user_id) |> Repo.one
    IO.inspect post
    if (Repo.preload(user,:followers)) do
                  IO.inspect "here....."
                  followers =(Repo.preload(user,:followers)).followers |> Enum.map(fn x ->  User |> where(id: ^(Map.get(x,:follower))) |>Repo.one end)
                  IO.inspect "followers "
                  IO.inspect followers
    else
        followers = []
    end

    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    conn |> put_status(200) |> json(%{"post": post, "user": post.user, "followers": followers})
  end

  def delete_post(conn, %{"id" => id}) do
    post = Repo.get Zencil.Post,id

    {status, data} = post
      |> cast(%{"status": "deleted"}, [:status])
      |> Repo.update

    IO.inspect data
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    json conn, %{}
  end

  def discard_post(conn, %{"id" => id}) do
    post = Repo.get Zencil.Post,id

    {status, data} = post
      |> cast(%{"status": "discarded"}, [:status])
      |> Repo.update

    IO.inspect data
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    json conn, %{}
  end

  def approve_post(conn, %{"id" => id}) do
    post = Repo.get Zencil.Post,id

    {status, data} = post
                      |> cast(%{"status": "published", "views": 0, "shares": 0}, [:status, :views, :shares])
                      |> Repo.update

    IO.inspect data
    conn = conn
            |> Plug.Conn.put_resp_header("Access-Control-Allow-Origin", "*")
            |> Plug.Conn.put_resp_header("X-Total-Count","1")
    json conn, %{}
  end

end

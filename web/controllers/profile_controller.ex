defmodule Zencil.ProfileController do
  use Zencil.Web, :controller
  import Ecto.Query
  import Ecto.Changeset
  alias Zencil.{Repo, User,FollowersList,PostLog}
  import Logger

  def get_profile(conn,%{"id" => id}) do
      IO.inspect "getting profile"
      {user_id,_} = Integer.parse(id)
      user = User |> where(id: ^user_id) |> Repo.one
      IO.inspect "alert... incoming"
      IO.inspect Repo.preload(user,:followers)|>Repo.preload(:following)
      if (Repo.preload(user,:followers)) do
                      followers =(Repo.preload(user,:followers)).followers |> Enum.map(fn x ->  User |> where(id: ^(Map.get(x,:follower))) |>Repo.one end)
      else
        followers = []
      end
      if (Repo.preload(user,:following)) do
                      following =(Repo.preload(user,:following)).following |> Enum.map(fn x -> User |> where(id: ^(Map.get(x,:owner))) |>Repo.one end)
      else
        following = []
      end
      IO.inspect followers
      IO.inspect following
      IO.inspect user
      conn |> put_status(200) |> json(%{"user": user, "followers": followers, "following": following})
  end

  def generate_password_hash(email_id, password) do
     :crypto.hash(:sha256, "#{password}{#{email_id}}") |> Base.encode64
  end

  def update_password(conn,_params) do
    IO.inspect _params
    email_id = _params["email_id"]
    old_password = _params["old_password"]
    new_password = _params["new_password"]
    confirm_passowrd = _params["confirm_passowrd"]
    IO.inspect email_id
    IO.inspect old_password
    IO.inspect generate_password_hash(email_id,old_password)
      user = User |> where(email_id: ^email_id) |> where(password: ^generate_password_hash(email_id,old_password))|> Repo.one
      IO.inspect user
      if user do
        {status,updated_user} = user |> cast(%{"password" => generate_password_hash(email_id,new_password)},[:password]) |> Repo.update
        IO.inspect "updated "
        IO.inspect status
        IO.inspect new_password
        IO.inspect updated_user
        case status do
          :ok ->         json conn,updated_user
          :error ->     conn |> put_status(400) |> json(%{"status": "failed"})
        end
      else
        conn |> put_status(201) |> json(%{"status": "password wrong"})
      end
  end

  def unfollow_profile(conn, %{"current_id" => current_id, "profile_id" => profile_id}) do
    {curr_id,_} = Integer.parse(current_id)
    {prof_id,_} = Integer.parse(profile_id)
    user = FollowersList |> where(owner: ^profile_id) |> where(follower: ^current_id) |> Repo.one
    Logger.info "Data found shall i delete?"
    Logger.info "debugging ...."
    case Repo.delete(user) do
      {:ok,data} -> conn |> put_status(200) |> json(%{"status": "success"})
       _-> conn |> put_status(400) |> json(%{"status": "failed"})
    end
  end

  def follow_profile(conn, %{"current_id" => current_id, "profile_id" => profile_id}) do
    {curr_id,_} = Integer.parse(current_id)
    {prof_id,_} = Integer.parse(profile_id)
    data = %FollowersList{}
              |> cast(%{"owner" => profile_id, "follower" => current_id },[:owner,:follower])
              |> validate_required([:owner,:follower])
              |> Repo.insert!
    IO.inspect data
    if data do
      conn |> put_status(200) |> json(%{"status": "success"})
    else
      conn |> put_status(400) |> json(%{"status": "failed"})
    end

  end

  def get_profiles(conn,%{"criteria" => criteria}) do
      case criteria do
        "followers" -> profiles = FollowersList
                                    |> group_by([p], p.owner)
                                    |> select([p],{p.owner, count(p.owner)})
                                    |> Repo.all
                                    |> Enum.map(fn {k, v} -> {v, k} end)
                                    |> Enum.sort
                                    |> Enum.reverse
                                    |> Enum.map(fn{k,v} -> User |> where(id: ^v) |>Repo.one
                                        end)
          render conn , profiles: profiles
        "all" -> profiles = User |> Repo.all
        render conn , profiles: [%{"title": "People","data": profiles}]
                  conn |> put_status(200) |> json(%{"title": "Profile","data": profiles})
        _-> conn |> put_status(400) |> json(%{"status": "failed"})
      end
  end

  def update_profile(conn,%{"id" => id,"age" => age, "location" => location, "occupation" => occupation, "username" => username}) do

    IO.inspect  User |> where(id: ^id) |> Repo.one
    {status, data} =  User |> where(id: ^id) |> Repo.one
        |> cast(%{"age" => age, "location" => location, "occupation" => occupation, "username" => username}, [:age, :location,:occupation, :username])
        |> Repo.update

    IO.inspect "update successful ? "
    IO.inspect status
    case status do
      :ok -> conn |> put_status(200) |> json(data)
      _-> conn |> put_status(400) |> json(%{"status": "failed"})
    end
  end

end

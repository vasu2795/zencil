defmodule Zencil.AuditLog do
  use Ecto.Schema
  alias Zencil.{AuditLog, Repo}
  import Ecto.Changeset

  @moduledoc """
  Audit trail for authenticaton related events. This must be an append only table

  SQL

  CREATE TABLE `audit_log` (
    id int NOT NULL AUTO_INCREMENT,
    target VARCHAR(255),
    target_entity VARCHAR(255),
    event VARCHAR(255),
    hostname VARCHAR(255),
    date_created DATETIME,
    PRIMARY KEY(id)
  );

  Here hostname is the name of the instance that serviced the request
  """
  schema "audit_log" do
    field :target, :string
    field :target_entity, :string
    field :event, :string
    field :hostname, :string

    timestamps([{:inserted_at, :date_created}, {:updated_at, false}])
  end

  defp add_host_and_save(log) do
    {status, hostname} = :inet.gethostname

    log |> cast(%{"hostname": List.to_string(hostname)}, [:hostname]) |> Repo.insert
  end

  def track_login(user_id) do
    user_id = Integer.to_string user_id

    log = %AuditLog{}
    |> cast(%{target: user_id, target_entity: "user", event: "login"}, [:target, :target_entity, :event])
    |> add_host_and_save
  end

  def track_logout(user_id) do
    user_id = Integer.to_string user_id

    log = %AuditLog{}
    |> cast(%{target: user_id, target_entity: "user", event: "logout"}, [:target, :target_entity, :event])
    |> add_host_and_save
  end

  def track_password_reset(user_id) do
    user_id = Integer.to_string user_id

    log = %AuditLog{}
    |> cast(%{target: user_id, target_entity: "user", event: "password_reset"}, [:target, :target_entity, :event])
    |> add_host_and_save
  end

  def track_forgot_password(user_id) do
    user_id = Integer.to_string user_id

    log = %AuditLog{}
    |> cast(%{target: user_id, target_entity: "user", event: "forgot_password"}, [:target, :target_entity, :event])
    |> add_host_and_save
  end

end

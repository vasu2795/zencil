defmodule Zencil.Category do

  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset

  alias Zencil.{Repo, User}

  @derive{Poison.Encoder, only: [:id, :category]}
  schema "category" do
    field :category,          :string
  end

end

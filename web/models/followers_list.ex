defmodule Zencil.FollowersList do

  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset

  alias Zencil.{Repo, User}
  @derive{Poison.Encoder, only: [:owner, :follower]}
  @primary_key {:follower, :integer, []}
  schema "follow_list" do
    belongs_to :user, Zencil.User, foreign_key: :owner
  end

end

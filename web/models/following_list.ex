defmodule Zencil.FollowingList do

  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset

  @primary_key {:owner, :integer, []}
  schema "follow_list" do
    belongs_to :user, Zencil.User, foreign_key: :follower
  end

end

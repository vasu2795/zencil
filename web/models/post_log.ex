defmodule Zencil.PostLog do
  use Zencil.Web, :model
  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset


  alias Zencil.{Repo}

  @derive{Poison.Encoder, only: [:id,:post_id, :client_id, :view, :liked ]}
  schema "post_log" do
    field :post_id, :integer
    field :client_id, :integer
    field :view, :boolean
    field :liked, :boolean
  end

end

defmodule Zencil.Post do
  use Zencil.Web, :model
  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset


  alias Zencil.{Repo}
  @derive{Poison.Encoder, only: [:id, :title, :category, :coverpic, :status, :user_id, :date_created,:username, :content ]}
  schema "posts" do
    field :title, :string
    field :category, :string
    field :coverpic, :string
    field :status, :string
    field :user_id, :integer
    field :username, :string
    field :content , :string
    timestamps([{:inserted_at, :date_created}, {:updated_at, :last_updated}])
    has_one :user, Zencil.User, foreign_key: :id, references: :user_id
    has_many :post_log, Zencil.PostLog, foreign_key: :post_id, references: :id
  end

  def sorted(query,param) do
    IO.inspect "params "
    IO.inspect param
    direction = :desc
    from p in query,
    order_by: [{^direction, field(p, ^param)}]
  end

end

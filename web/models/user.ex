defmodule Zencil.User do
  @doc """
  added one new column
    alter table user add column is_password_changed boolean;
  """
  use Ecto.Schema
  import Ecto.Query
  import Ecto.Changeset

  alias Zencil.{Repo, User,FollowersList,FollowingList}

  @derive{Poison.Encoder, only: [:id, :email_id, :username, :age, :location, :occupation, :role, :image_url, :advanced_id, :signup_method, :googleid, :googleimgurl, :facebookid, :facebookimageurl]}
  schema "users" do
    field :password,          :string
    field :username,          :string
    field :age,               :integer
    field :advanced_id,       :integer
    field :location,          :string
    field :occupation,        :string
    field :role,              :string
    field :email_id,          :string
    field :image_url,         :string
    field :signup_method,     :string
    field :googleid,          :string
    field :googleimgurl,      :string
    field :facebookid,        :string
    field :facebookimageurl,  :string
    field :image_type,        :string
    has_many :followers, FollowersList, foreign_key: :owner, on_delete: :delete_all
    has_many :following, FollowingList, foreign_key: :follower, on_delete: :delete_all
  end

  def authenticate_user(email_id, password) do
    password_hash = generate_password_hash(email_id, password)
    IO.inspect "here to authenticate"
    user = User
      |> where(email_id: ^email_id)
      |> where(password: ^password_hash)
      |> Repo.one

    if user do
      {:ok,user}
    else
      "invalid_creds"
    end
  end

  def generate_password_hash(email_id, password) do
     :crypto.hash(:sha256, "#{password}{#{email_id}}") |> Base.encode64
  end

  def signup_google(email_id,id,name,image_url) do
      IO.inspect "google signup here...."

    user = %User{}
            |> cast(%{"password" => generate_password_hash(email_id,email_id),"email_id" => email_id, "username" => name,"image_url" => image_url, "googleid" => id},[:password,:email_id,:username,:googleimgurl, :googleid])
            |> validate_required([:password,:email_id, :username, :googleimgurl,:googleid])
            |> Repo.insert!
    IO.inspect "signup insert"
    IO.inspect user
    if user do
      user
    else
       "google_auth_fail"
    end
  end

  def google_authenticate(email_id,id,name,image_url) do
    IO.inspect "here to authenticate with google"
    user = User
      |> where(email_id: ^email_id)
      |> Repo.one

    if user do
      if user.googleid do
        user
      else
        user = user |> cast(%{"googleid" => id, "googleimgurl" => image_url},[:googleid,:googleimgurl]) |> Repo.update!
        IO.inspect "user after insert"
        user
      end    else
      signup_google(email_id,id,name,image_url)
    end

  end

  def facebook_authenticate(email_id,id,name,image_url) do
    user = User
      |> where(email_id: ^email_id)
      |> Repo.one

    if user do
      if user.facebookid do
        user
      else
        user = user |> cast(%{"facebookid" => id, "facebookimageurl" => image_url},[:facebookid,:facebookimageurl]) |> Repo.update!
        IO.inspect "user after insert"
        user
      end
    else
      signup_facebook(email_id,id,name,image_url)
    end

  end

  def signup_facebook(email_id,id,name,image_url) do
    IO.inspect "facebook signup here...."
    user = %User{}
            |> cast(%{"password" => generate_password_hash(email_id,email_id),"email_id" => email_id, "username" => name,"facebookimageurl" => image_url, "facebookid" => id},[:password,:email_id,:username,:facebookimageurl, :facebookid])
            |> validate_required([:password,:email_id, :username, :facebookimageurl,:facebookid])
            |> Repo.insert!
    IO.inspect "signup insert"
    IO.inspect user
    if user do
      user
    else
       "facebook_auth_fail"
    end
  end

end

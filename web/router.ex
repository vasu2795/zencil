defmodule Zencil.Router do
  use Zencil.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :auth do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  pipeline :api do
    plug :accepts, ["json"]
  end


  scope "/auth", Zencil do
    pipe_through :auth

    post "/login", AuthController, :login
    post "/signup/:email_id", AuthController, :sign_up
    post "/login/:email_id", AuthController, :login
    post "/google", AuthController, :google_authenticate
    post "/facebook", AuthController, :facebook_authenticate

  end

  scope "/", Zencil do
    pipe_through :api
    get "/post/:id/data", PostController, :get_post
    get "/post/:id/:profile_id/data", PostController, :load_article
    post "/posts", PostController, :list_posts
    post "/profile", ProfileController, :get_profiles
    post "/post/:post_id/:profile_id/like", PostController, :like_post
    post "/post/:post_id/:profile_id/unlike", PostController, :unlike_post

  end

  scope "/profile", Zencil do
    pipe_through :auth
    get "/:id/data", ProfileController, :get_profile
    post "/:current_id/:profile_id/unfollow" , ProfileController, :unfollow_profile
    post "/:current_id/:profile_id/follow" , ProfileController, :follow_profile
    post "/:id/update", ProfileController, :update_profile
    post "/:email_id/password_update", ProfileController, :update_password
  end

  scope "/", Zencil do
    pipe_through :browser # Use the default browser stack
    get "/", PageController, :index
    get "/*path", RedirectController, :redirector
  end

  # Other scopes may use custom stacks.
  # scope "/api", Zencil do
  #   pipe_through :api
  # end
end

defmodule Zencil.RedirectController do
  use Zencil.Web, :controller
  @send_to "/app"

  def redirector(conn, _params) do
      IO.inspect "redirecting"
      redirect(conn, to: @send_to)
   end
end

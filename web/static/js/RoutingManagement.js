import store from './store/AppStore';
import { hashHistory } from 'react-router';

export default function dispatchNavigationEvent(to) {
  hashHistory.push(to);

  store.dispatch({
    type: 'NAVIGATION_ACTION',
    to: to
  });
}

// import "phoenix_html";

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import ReactDOM from 'react-dom';
import React from 'react';
import axios from 'axios';

import store from './store/AppStore';
import { initApp } from './actions/AuthActions';

import LoginForm from './components/LoginForm';
import ResetForm from './components/ResetForm';
import MainApp from './components/MainApp';
import Feed from  './components/Feed';
import Profile from './components/Profile';
import Article from './components/Article';
import CryptoJS from 'crypto-js'

import {isLoggedIn} from './components/Utils'
import { Router, Redirect,Route, IndexRoute, hashHistory } from 'react-router';

axios.defaults.headers.common['Version'] = '2016-07-19';
let currentUrl = window.location.hash.split("/#").join('').split("#").join('');
console.log("current url is",currentUrl)
if(!currentUrl.includes("/reset")){
  if(!isLoggedIn()) {
      console.log(window.location.hash)
      sessionStorage.setItem('redirectTo',currentUrl)
      hashHistory.push('/login')
  }
}

hashHistory.listen((args) => {
  console.log("args is ",args)
  let simplePath = args.pathname.split('/').join('');
  let currentSimplePath = store.getState()['Navigation'].lastVisited.split('/').join('');
  console.log("simple path " ,simplePath)
  console.log("current simple path ",currentSimplePath)
  if(simplePath != currentSimplePath) {
    console.log("dispatching",args.pathname)
		/*
			Do not call dispatchNavigationEvent here.
			Dispatch navigation event will update the router history ->
			and the snake eats itself.
		 */
    store.dispatch({
      type: 'NAVIGATION_ACTION',
      to: args.pathname
    });
  }
});

ReactDOM.render(
	<Router history={hashHistory}>
		<Route path="/login" component={LoginForm} />
    <Route path="/app" component={MainApp}>
      <Route path="/login" component={Feed} />
      <Route path= "/profile/:id/data" component={Profile} />
      <Route path= "/#/profile/:id/data" component={Profile} />
      <Route path= "/post/:id/view" component={Article} />
      <Route path= "/" component={Feed} />
      <Redirect from= "/*" to="/" />
    </Route>
	</Router>, document.getElementById('application'));

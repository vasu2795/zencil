import React from 'react';
import axios from 'axios';
import Avatar from 'react-avatar';
import moment from 'moment';
import dispatchNavigationEvent from '../RoutingManagement';
import R from 'ramda';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import Instagram from 'react-share-icons/lib/Instagram';
import {getCurrentId} from './Utils'
import {
  ShareButtons,
  ShareCounts,
  generateShareIcon
} from 'react-share';
const {
  FacebookShareButton,
  GooglePlusShareButton,
  TwitterShareButton,
  } = ShareButtons;
const {
  FacebookShareCount,
  GooglePlusShareCount,
} = ShareCounts;

const FacebookIcon = generateShareIcon('facebook');
const GooglePlusIcon = generateShareIcon('google');
const TwitterIcon = generateShareIcon('twitter');

const Tag = ({text}) => {
  return (
    <div style={{display: 'inline-block',marginLeft: '10px',paddingTop: '5px',paddingLeft: '5px',paddingLeft:'8px',paddingRight: '8px',border: '1px solid #56A4FF',borderRadius: '1px'}}>
      <div style={{display: 'inline-block'}}>{text}</div>
      <img style={{marginLeft: '10px',display: 'inline-block'}} src="images/close.png"/>
    </div>
  );
}

export default class Article extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      id: null,
      user_id: null,
      username: null,
      content: null,
      category: null,
      coverpic: null,
      date_created: null,
      status: null,
      title: null,
      isFollowing: null,
      current_id: null,
      self: null,
      user: null,
      isLiked: false,
      likes: null
    }
  }

  componentDidMount() {
    console.log("props is ",this.props);
    this.initiate();
  }

  componentWillReceiveProps(newProps) {
    console.log("new props is ",newProps);
      if(this.props != newProps) {
        this.props = newProps;
        this.initiate();
      }
  }

  onAction = () => {
    console.log("on action ",this.state)
    if(!this.state.self) {
      if(this.state.isFollowing) {
        axios.post(`profile/${this.state.current_id.toString()}/${this.state.user_id}/unfollow`).then((response) => {
          console.log("here... unfollowing",response)
          if(response.status == 200) {
            console.log("dispatching")
            dispatchNavigationEvent(`/post/${this.state.id}/view`);
          }
        }).catch((error) => {
          console.log("error unfollowing ");
        })
      } else {
        axios.post(`profile/${this.state.current_id.toString()}/${this.state.user_id}/follow`).then((response) => {
          console.log("here... following",response)
          if(response.status == 200) {
            console.log("dispatching")
            dispatchNavigationEvent(`/post/${this.state.id}/view`);
          }
        }).catch((error) => {
          console.log("error following ");
        })
      }
    }
  }

  likePost = () => {
    axios.post(`post/${this.state.id}/${getCurrentId()}/like`).then((response) => {
      this.state.isLiked = true;
      this.state.likes = this.state.likes + 1;
      this.setState(this.state);
    }).catch((error) => {

    });
  }
  unlikePost = () => {
    axios.post(`post/${this.state.id}/${getCurrentId()}/unlike`).then((response) => {
      this.state.isLiked = false;
      this.state.likes = this.state.likes - 1;
      this.setState(this.state);
    }).catch((error) => {

    });
  }

  initiate = () => {
    axios.get(`/post/${this.props.params.id}/${getCurrentId()}/data`).then((response) => {
      console.log(" response data is ",response.data)
      this.state.current_id = getCurrentId();
      this.state.views = response.data.views.length;
      this.state.likes = response.data.likes.length;
      console.log("likes ",response.data.likes)
      if(R.any((element) => { console.log("element now is ",element.toString() == getCurrentId()); return element.toString() == getCurrentId()},response.data.likes)) {
        this.state.isLiked = true;
      } else {
        this.state.isLiked = false;
      }
      console.log("state now is ",this.state);
      if(this.state.current_id ==  response.data.user_id) {
        this.state.self = true;
      } else {
        this.state.self = false;
      }
      console.log("current id is ",this.state.current_id);
      this.state = R.merge(this.state,response.data.post);
      this.state.user  = response.data.user;
      console.log("this state ");
      if(R.any((element) => { console.log("element now is ",element); return element.id == this.state.current_id},response.data.followers) ) {
        console.log("is following")
        this.state.isFollowing = true;
      } else {
        console.log("not following");
        this.state.isFollowing = false;
      }
      this.setState(this.state);
    }).catch((error) => {
      console.log("Error is ",error)
    })
  }

  render() {
    console.log("state received is ",this.state)
    if(!this.state.user) {
      return (<div className="ui basic segment enterframe loading">
      </div>
  );
    }
    return (
      <div  style={{
        paddingLeft: '11.71%',paddingRight:'15.71%',paddingTop: '50px',overflow: 'auto',paddingBottom: '50px'
      }}>
      <div style={{cursor:'pointer',minHeight: '50px', height: '8.15%',display: 'table',marginBottom: '50px'}}>
        <div style={{display: 'table-cell',verticalAlign: 'middle'}}>
          <Avatar size="80" round='true' facebook-id="invalidfacebookusername" src="images/mark.png" />
        </div>
        <div style = {{display: 'table-cell',verticalAlign: 'middle',paddingLeft: '20px',paddingRight: '20px'}}>
          <div style={{display:'table-row',verticalAlign: 'middle'}}>
            <a  style={{fontFamily: 'Avenir-Roman',fontSize: '22px',color: '#666666',letterSpacing: '1px'}} > {this.state.username} </a>
            <MuiThemeProvider>
            <RaisedButton onTouchTap={::this.onAction} style={{marginLeft: '50px',height: '100%',color: "#FFFFFF"}}  labelColor='#FFFFFF' buttonStyle= {{outline: 'none',padding: '5px',fontFamily: 'Avenir-Book' ,cursor: 'pointer',fontSize: '14px'  ,background: '#56A4FF', border: '1px solid #56A4FF', borderRadius: '1px',color: '#FFFFFF',letterSpacing: '0.7px',width: '100%',height: '100%'}}  label={this.state.isFollowing ? "UnFollow" : "Follow"} />
            </MuiThemeProvider>
          </div>
          <div  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow: 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '14px',color: '#666666',letterSpacing: '0.8px'}} ><div style={{paddingTop: '5px'}}>{this.state.user.age ? (this.state.user.age + (this.state.user.occupation ? ("," + this.state.user.occupation) : "")) : (this.state.user.occupation ? this.state.user.occupation : (this.state.user.location ? this.state.user.location : this.state.user.email_id)) }</div></div>
        </div>
      </div>

        <img width="800px" height="300px" src="images/test2.png" />
        <div style={{marginTop: '36px',fontFamily: 'Avenir-Medium',
fontSize:'36px',
color: '#666666',
letterSpacing: '1.8px'}}> Content Content Content Content</div>
        <div style={{paddingTop:'10px',display: 'flex',fontFamily: 'Avenir-BookOblique',fontSize: '14px',color: '#999999',letterSpacing: '0.7px'}}>
          <div style={{}}>Published on {moment.utc(new Date(this.state.date_created)).format('LL')} </div>
          <img style={{display:'inline-block',marginLeft: '60px'}} src="images/eye.png" />
          <div style={{display:'inline-block',marginLeft: '10px'}}>{this.state.views}</div>
        </div>
        <div style={{paddingTop: '36px',fontFamily: 'Avenir-Book',fontSize: '18px',color: '#666666',letterSpacing: '0.5px'}}>
          <p>I distinctly recall one day when I was 12 years old turning on MTV and finding not the music video I was looking for, but instead news that Kurt Cobain had killed himself. Obviously, that would be confusing to any young kid. But it was especially confusing to a young kid who had just started getting into music.</p>
          <p style={{marginTop:'36px'}}>It’s honestly probably part of the reason why I was never that into Nirvana. Don’t get me wrong, they were amazing pioneers and there’s no greater testament to their legacy than to say that their songs stand the test of time. But even though I was a teenager in the 1990s and I was very into “grunge” music, Nirvana wasn’t my go-to. Two other Seattle bands were: Pearl Jam and Soundgarden.</p>
          <img width='500px' height='200px' style={{marginTop:'36px'}} src="images/test.png" />
          <p style={{marginTop:'36px'}}>It’s honestly probably part of the reason why I was never that into Nirvana. Don’t get me wrong, they were amazing pioneers and there’s no greater testament to their legacy than to say that their songs stand the test of time. But even though I was a teenager in the 1990s and I was very into “grunge” music, Nirvana wasn’t my go-to. Two other Seattle bands were: Pearl Jam and Soundgarden.</p>
          <p style={{marginTop:'36px'}}>It’s honestly probably part of the reason why I was never that into Nirvana. Don’t get me wrong, they were amazing pioneers and there’s no greater testament to their legacy than to say that their songs stand the test of time. But even though I was a teenager in the 1990s and I was very into “grunge” music, Nirvana wasn’t my go-to. Two other Seattle bands were: Pearl Jam and Soundgarden.</p>
          <p style={{marginTop:'36px'}}>It’s honestly probably part of the reason why I was never that into Nirvana. Don’t get me wrong, they were amazing pioneers and there’s no greater testament to their legacy than to say that their songs stand the test of time. But even though I was a teenager in the 1990s and I was very into “grunge” music, Nirvana wasn’t my go-to. Two other Seattle bands were: Pearl Jam and Soundgarden.</p>
          <div style={{marginTop: '36px',display: 'flex',fontFamily: 'Avenir-Book',fontSize: '12px',color: '#56A4FF',letterSpacing: '0.6px'}}>
            <Tag text="Food" />
            <Tag text="Sample" />
            <Tag text="Test" />
          </div>
          <div style={{verticalAlign: 'middle',marginTop: '36px',display: 'flex',fontFamily: 'Avenir-Book',fontSize: '18px',color: '#666666',letterSpacing: '1px'}}>
            {
                do {
                  if(this.state.isLiked) {
                    <div onClick = {::this.unlikePost} style={{display: 'inline-block',paddingTop:'4px',cursor: 'pointer'}}><img src="images/hr.png" /></div>
                  } else {
                    <div onClick = {::this.likePost} style={{display: 'inline-block',paddingTop:'4px',cursor: 'pointer'}}><img src="images/hw.png" /></div>
                  }
                }
            }
            <div style={{display: 'inline-block',marginLeft: '10px',paddingTop: '7px'}}>{this.state.likes}</div>
            <div style={{display: 'inline-block', width: '50px'}}> </div>
            <div style={{display: 'inline-block',cursor:'pointer'}}> <FacebookShareButton  description = "test attempt" title={"Zencil - Post"}  url = {"https://zencil.herokuapp.com/#/post/"+this.props.params.id+"/view"} children=""> <img src="images/3.png" />  </FacebookShareButton> </div>
            <div style={{display: 'inline-block', width: '8px'}}> </div>
            <div style={{display: 'inline-block',cursor:'pointer'}}> <GooglePlusShareButton   url = {"https://zencil.herokuapp.com/#/post/"+this.props.params.id+"/view"} children=""> <img src="images/1.png" />  </GooglePlusShareButton> </div>
            <div style={{display: 'inline-block', width: '8px'}}> </div>
            <div style={{display: 'inline-block',cursor:'pointer'}}> <TwitterShareButton  title={"Zencil - Post"} via="zencil" hashtags= { ["TEST", "zencil"] }  url={"https://zencil.herokuapp.com/#/post/"+this.props.params.id+"/view"} children=""> <img src="images/2.png" />  </TwitterShareButton> </div>
          </div>

        </div>
      </div>
    )
  }
}

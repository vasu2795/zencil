import React from 'react';
import R from 'ramda';
import axios from 'axios';
import Avatar from 'react-avatar';
import dispatchNavigationEvent from '../RoutingManagement';
import moment from 'moment';
import Loading from 'react-loading';

// import AdBlockDetect from 'react-ad-block-detect';
// import $ from 'jquery';
//
// class MyComponent extends React.Component {
//     componentDidMount() {
//
//     }
//     render() {
//         return (
//             <AdBlockDetect>
//               <div className="ui visible message">
//                 <div className="header">
//                   Hey there!
//                 </div>
//                 <p> Adblock has been enabled in your browser. You might not be able to view social media share icons in this site as adblock extensions misinterpret facebook,twitter share icons as ads. </p>
//                 <p> Please disable your adblock (or) whitelist this site or) use Incognito mode for better experience.</p>
//               </div>
//             </AdBlockDetect>
//         );
//     }
// }

class Leftbar extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      category: this.props.category
    }
  }

  componentDidMount() {
  }

  onSelect = (id) => {
    console.log("id selected ",id)
    this.state.category = id;
    this.setState(this.state);
    this.props.onSelect(this.state);
  }

  render() {

    return (
      <div style={{width : '20%', height: '100%',background: 'rgb(243, 243, 243)'}} >
      <div style={{display: 'table',height: '100%',width: '100%'}}>
      {R.addIndex(R.map)((logic, index) => {
                      console.log("logic and index are ",logic,index)
                      let bgcolor = (this.state.category == index+1 ? "#56A4FF" : "transparent" );
                      let font_size = (this.state.category == index+1 ? "14px" : "12px");
                      // let margin_top = (this.state.category == index ? "14px" : "12px");
                      return (
                        <div style={{cursor: 'pointer',display: 'table-row',fontSize: font_size,height:'7.40%','cursor': 'pointer', background: bgcolor, fontFamily: 'Avenir-Book',verticalAlign: 'middle',letterSpacing: '0.7px'}}>
                          <a style={{color:(this.state.category == index+1 ? "#FFFFFF":'#999999'),display: 'table-cell',verticalAlign: 'middle',paddingLeft:'18.1%'}} onClick={() => ::this.onSelect(index+1)}> {logic} </a>
                        </div>
                      );
                    }, ["All","Health,Science & Tech","News,Social & History","Adult","Personal","Business","Others"])}
                    <div style={{display:'table-row',height: '10.37%'}}> </div>
                    <div style={{cursor: 'pointer',display: 'table-row',fontSize: "12px",height:'7.40%','cursor': 'pointer', background: (this.state.category == 9 ? "#56A4FF" : "transparent" ), fontFamily: 'Avenir-Book',verticalAlign: 'middle',letterSpacing: '0.7px'}}>
                      <a style={{color:(this.state.category == 9 ? "#FFFFFF":'#999999'),display: 'table-cell',verticalAlign: 'middle',paddingLeft:'18.1%'}} onClick={() => ::this.onSelect(9)}> About Us </a>
                    </div>
                    <div style={{cursor: 'pointer',display: 'table-row',fontSize: "12px",height:'7.40%','cursor': 'pointer', background: (this.state.category == 10 ? "#56A4FF" : "transparent" ), fontFamily: 'Avenir-Book',verticalAlign: 'middle',letterSpacing: '0.7px'}}>
                      <a style={{color:(this.state.category == 10 ? "#FFFFFF":'#999999'),display: 'table-cell',verticalAlign: 'middle',paddingLeft:'18.1%'}} onClick={() => ::this.onSelect(10)}> Contact </a>
                    </div>
                    <div style={{display: 'table-row',height:'7.40%'}} />
                    <div style={{display:'table-row'}}>
                        <div style={{display: 'table-cell',verticalAlign: 'middle',paddingLeft:'18.1%'}}>
                        <a style={{'cursor':'pointer'}} target="_blank" href="https://www.facebook.com/blogroads/" > <img src="images/instaa_logo.png" /> </a>
                        <a id="facebook" style={{'cursor':'pointer','margin-left': '10px'}} target="_blank" href="https://www.facebook.com/blogroads/" > <img src="images/instaaa_logo.png" /> </a>
                        <a style={{'cursor':'pointer','margin-left': '10px'}} target="_blank" href="https://www.facebook.com/blogroads/" > <img src="images/insta_logo.png" /> </a>
                        </div>
                    </div>
                  </div>
                 </div>
    );
  }
}

class Rightbar extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      posts: [],
      people: [],
      posts_loaded: false,
      people_loaded: false
    }
  }

  componentDidMount() {
      this.loadTrending();
      this.loadPeople();
  }

  loadPeople() {
     console.log("loading people")
      axios.post(`/profile`,{criteria: "followers"}).then((response) => {
        console.log("people is ",response.data);
        this.state.people = response.data
        this.state.people_loaded = true;
        this.setState(this.state)
      }).catch((error) => {
        console.log("Error is ",error);
      })
    }

  loadTrending() {
    axios.post(`/posts`,{category: 1, sortBy: "Trending", status: "published"}).then((response) => {
      console.log("Response is ",response.data);
      this.state.posts = response.data
      this.state.posts_loaded = true;
      this.setState(this.state)
    }).catch((error) => {
      console.log("Error is ",error);
    })
  }

  render() {
    // if(this.state.posts_loaded && this.state.people_loaded) {

      return (
        <div style={{background: '#F3F3F3',width : '21.5%', height: '100%',display: 'inline-block',paddingLeft: '1.5625%'}} >
        <div style = {{position:'relative',height: '8.15%',width:'100%',borderBottom: '1px solid #D5D5D5', fontFamily: 'Avenir-Medium',fontSize: '14px',color: '#999999',letterSpacing: '0.7px'}} ><div style={{position: 'absolute','top':'50%',transform: 'translate(0, -50%)'}}>TRENDING POSTS</div> </div>
        <div style={{overflowX: 'auto',overflowy: 'auto',maxHeight: '44.6%'}} >
        {R.map((post) => {
          return(
            <div onClick= {() => dispatchNavigationEvent(`post/3/view`)} style={{cursor: 'pointer',minHeight: '50px', height: '8.15%',display: 'table'}}>
              <div style={{display: 'table-cell',verticalAlign: 'middle'}}>
                <Avatar size="30" facebook-id="invalidfacebookusername" src="images/test.png" />
              </div>
              <div style = {{display: 'table-cell',verticalAlign: 'middle',paddingLeft: '20px',paddingRight: '20px'}}>
                <div style={{overflow: 'hidden','display': 'table','table-layout': 'fixed',width: '100%'}} >
                  <div  style={{textAlign: 'justify',display: 'table-cell',overflow: 'hidden','word-break': 'break-all','word-wrap': 'break-word',textOverflow: 'ellipsis',whiteSpace: 'nowrap', fontFamily: 'Avenir-Roman',fontSize: '10px',color: '#666666',letterSpacing: '0'}}>Lorem Ipsum Caratta Magnum asdadsasdsadasdadsadads</div>
                </div>
                <div  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden','textOverflow': 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '10px',color: '#666666',letterSpacing: '0'}} > {moment.utc(new Date(post.date_created)).format('LL')}</div>
              </div>
            </div>
          );
        },this.state.posts )}
        </div>
        <div style = {{marginTop: '5.55%',position:'relative',height: '8.15%',width:'100%',borderBottom: '1px solid #D5D5D5', fontFamily: 'Avenir-Medium',fontSize: '14px',color: '#999999',letterSpacing: '0.7px'}} ><div style={{position: 'absolute','top':'50%',transform: 'translate(0, -50%)'}}>PEOPLE</div> </div>
        <div style={{overflowX: 'auto',overflowY: 'auto','maxHeight': '32.6%'}} >
        {R.map((people) => {
          console.log("people is.... ",people);
          let image_url = people.facebookimageurl ? people.facebookimageurl : (people.googleimgurl ? people.googleimgurl : "images/user.png")

          return(
            <div onClick= {() => dispatchNavigationEvent(`profile/${people.id}/data`)} style={{cursor:'pointer',minHeight: '50px', height: '8.15%',display: 'table'}}>
              <div style={{display: 'table-cell',verticalAlign: 'middle'}}>
                <Avatar size="30" round='true' facebook-id="invalidfacebookusername" src={image_url} />
              </div>
              <div style = {{display: 'table-cell',verticalAlign: 'middle',paddingLeft: '20px',paddingRight: '20px'}}>
                <a  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow : 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '10px',color: '#666666',letterSpacing: '0'}} > {people.username} </a>
                <div  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow: 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '10px',color: '#666666',letterSpacing: '0'}} >{people.age ? (people.age + (people.occupation ? ("," + people.occupation) : "")) : (people.occupation ? people.occupation : (people.location ? people.location : people.email_id)) }</div>
              </div>
            </div>
          );
        },this.state.people )}
        </div>
      </div>
      );
  }
}

class Card extends  React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
    }
  }
  componentDidMount() {
  }

  componentWillReceiveProps(newProps){
  }

  render() {
    return (
      <div onClick={() => {dispatchNavigationEvent(`post/3/view`)}} style={{cursor: 'pointer',display: 'table-cell',width: '47.5%',height:'100%',background: '#FFFFFF',boxShadow: '0 1px 2px 0 rgba(212,212,212,0.50)'}}>
        <div className="coverPic" style={{display: 'table',width:'100%',height: '66.35%',backgroundImage: 'url("../images/test.png")'}}>
        </div>
        <div style={{display:'table',height:'33.65%',width: '100%',verticalAlign:'middle',textAlign: 'center'}}>
            <div style={{paddingLeft: '5%',paddingRight: '5%',width:'100%',display: 'table-cell',verticalAlign:'middle'}}>
              <div style={{'display': 'table','table-layout': 'fixed',width: '100%'}} >
                <div  style={{textAlign: 'justify',display: 'table-cell',overflow: 'hidden','word-break': 'break-all','word-wrap': 'break-word',textOverflow: 'ellipsis',whiteSpace: 'nowrap', fontFamily: 'Avenir-Medium',fontSize: '12px',color: '#999999',letterSpacing: '1px'}}>LOREM IPSUM CARATTA MAGNUM ROBOTO</div>
              </div>
              <div  style={{paddingTop: '10px',fontFamily: 'Avenir-Medium',fontSize: '12px',color: '#999999',letterSpacing: '1px'}}>
                <div style={{float: 'left'}} > {moment.utc(new Date(this.props.post.date_created)).format('LL')} </div>
                <div style={{float: 'right',display:'flex'}} >
                  <img src="images/hearts.png" />
                  <div style={{display:'inline-block', marginLeft: '5px'}}>45</div>
                </div>
                <div style={{'display': 'table','table-layout': 'fixed',width:'50%','position': 'relative', 'margin': 'auto'}}>
                  <div  style={{textAlign: 'center',display: 'table-cell',overflow: 'hidden','word-break': 'break-all','word-wrap': 'break-word',textOverflow: 'ellipsis',whiteSpace: 'nowrap', fontFamily: 'Avenir-Medium',fontSize: '12px',color: '#999999',letterSpacing: '1px'}}>Rhodes</div>
                </div>
               </div>
            </div>
        </div>
      </div>
    );
  }
}
class PostList extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      category: 0,
      sortBy: "Latest",
      posts: [],
      tags: [],
      loading: false
    }
  }
  componentDidMount() {
    this.state.category = this.props.category;
    this.state.sortBy = this.props.sortBy;
    this.loadPosts();
  }

  componentWillReceiveProps(newProps){
    if((this.props.category != newProps.category) || (this.props.sortBy!=newProps.sortBy)) {
      this.state.category = newProps.category;
      this.state.sortBy = newProps.sortBy;
      this.loadPosts();
    }
  }

  loadPosts() {
    this.state.load = true;
    this.setState(this.sate);
    axios.post(`/posts`,{category: this.state.category, sortBy: this.state.sortBy, status: "published"}).then((response) => {
      console.log("Response is ",response.data);
      this.state.posts = response.data
      this.state.load = false;
      this.setState(this.state)
    }).catch((error) => {
      this.state.load = false;
      console.log("Error is ",error);
    })
  }

  render(){
    console.log("main list posts with ",this.state,this.props)
    let groupedPosts = R.groupWith((a,b)=> R.indexOf(a,this.state.posts) + 1 == R.indexOf(b,this.state.posts),this.state.posts);
    if(this.state.loading) {
      return (
        <Loading type={type} color={color} height='667' width='375' />
      );
    }
    return(
    <div style={{width: '58.5%',height: '100%', display: 'inline-block',overflowY: 'auto'}} >
      <div style={{height: '12.40%',display:'table'}}>
        {do {
          if(this.state.tags == []) {
            <div style={{display: 'table-cell'}}></div>
          }
          else {

          }
        }}
      </div>
      {
        R.map((posts) => {
          console.log("here... ",posts)
          return(
            <div style={{display: 'table',paddingLeft: '4.68%',paddingRight:'5.68%',width:'100%',height: '39.07%',marginBottom: '4.45%'}}>
                  {
                    do {
                      if(posts[1]){
                        <div style={{display:'table-row',width:'100%',height: '100%'}}>
                            <Card post= {posts[0]}/>
                            <div style={{display: 'table-cell',width: '5%'}}>  </div>
                            <Card post={posts[1]}/>
                        </div>

                      } else {
                        <div style={{display:'table-row',width:'100%',height: '100%'}}>
                            <Card post= {posts[0]}/>
                            <div style={{display: 'table-cell',width: '52.5%'}}>  </div>
                        </div>
                      }
                    }
                  }
            </div>);
        },groupedPosts)
      }
    </div> );
  }
}

export default class Feed extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      category: 1
    }
  }

  onSelect = (that) => {
    this.state.category = that.category;
    this.setState(this.state);
  }

  render() {
    console.log("renering.....",this.props)
    return (
      <div style={{ display: 'flex',height: '100%'}}>
        <Leftbar category={this.state.category} onSelect={this.onSelect} />
        <div style={{textAlign:'center',display:'inline-block',height: '100%'}} className="vrmain">&nbsp;</div>
        <PostList category={this.state.category} sortBy={this.props.sortBy}/>
        <Rightbar />
      </div>
    );
  }
}

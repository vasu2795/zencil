import React from 'react';
import store from '../store/AppStore';
import GoogleLogin from 'react-google-login';
import axios from 'axios';
import dispatchNavigationEvent from '../RoutingManagement';
import { initApp } from '../actions/AuthActions';
import { hashHistory } from 'react-router';
import CryptoJS from 'crypto-js';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Checkbox from 'material-ui/Checkbox';
import R from 'ramda'
import {isLoggedIn} from './Utils'
export default class LoginForm extends React.Component {
  constructor(props){
    super(props);
    this.props = props
    this.state = {
      signin: true,
      loginError: false,
      checked: false,
      empty_field_signup: false,
      terms_disagree: false,
      password_mismatch: false,
      invalid_signup_input: false,
      account_exists: false
    }
  }

  componentDidMount() {
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1848061888791160',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.8'
      });
      FB.AppEvents.logPageView();
    };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));


    if(isLoggedIn()) {
      store.dispatch(initApp());
      hashHistory.push('/app');
      dispatchNavigationEvent(`${sessionStorage.getItem('redirectTo')}`);
    }
  }


  statusChangeCallback = (response) => {
  console.log('statusChangeCallback');
  console.log(response);
  // The response object is returned with a status field that lets the
  // app know the current login status of the person.
  // Full docs on the response object can be found in the documentation
  // for FB.getLoginStatus().
  if (response.status === 'connected') {
    console.log("connected")
    // Logged into your app and Facebook.
    FB.api('/me?fields=name,email,id,birthday', function(response) {
      console.log('Successful me for: ',response);
      this.getimage(response);
      // this.facebookLogin(userID,response.data.url)
    }.bind(this));
  } else if (response.status === 'not_authorized') {
    // The person is logged into Facebook, but not your app.
    console.log("not authorized....");
  } else {
    // The person is not logged into Facebook, so we're not sure if
    // they are logged into this app or not.
    console.log("next place neither ....");
  }
}

getimage = (response) => {
  console.log('Welcome!  Fetching your information.... ',response);
  let userID = response.id;
  let name = response.name;
  let email = response.email;
  FB.api(`/${userID}/picture?width=200&height=200`, function(response) {
    console.log('Successful login for: ',response);
    this.facebookLogin(email,userID,name,response.data.url)
  }.bind(this));
}

checkLoginState = () => {
  FB.getLoginStatus(function(response) {
    this.statusChangeCallback(response);
  }.bind(this));
}

handleClick = () => {
  console.log("handle click...");
  FB.login(function(response) {
    console.log("login response....",response);
    this.statusChangeCallback(response);
  }.bind(this),{scope: 'public_profile,email,user_birthday'});
}

  onAction = (variable) => {
    console.log("on action",variable)
    this.state.empty_field_signup = false;
    this.state.terms_disagree = false;
    this.state.password_mismatch = false;
    this.state.invalid_signup_input = false;
    this.state.account_exists = false;
    this.state.loginError = false;
    this.state.signin = variable>0?true:false;
    this.setState(this.state);
  }
  //
  onCheck = (event)  => {
    console.log('onCheck: ', event); // always called
    this.state.checked = !this.state.checked;
  }

  doLogin = () => {
    console.log("login");
    this.state.empty_field_signup = false;
    this.state.terms_disagree = false;
    this.state.password_mismatch = false;
    this.state.invalid_signup_input = false;
    this.state.account_exists = false;
    this.state.loginError = false;
    var email_regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/ ;
    let email_id = this.refs.login_email.value;
    let password = this.refs.login_password.value;
    if(!password || !email_id || !email_regex.test(email_id) || password.length<5) {
      console.log("empty fields")
      this.state.loginError = true;
      this.setState(this.state)
    }
    else {
      this.state.loginError = false;
      axios.post(`/auth/login/${email_id}`,{password: password}).then((response) => {
        console.log("success !",response);
        if(response.status == 200) {
          console.log("here....",typeof(response.data.id));
          this.localLogin(response.data.email_id.toString(),response.data.id.toString())
          store.dispatch(initApp());
          console.log("logged in..... ",sessionStorage.getItem('redirectTo'))
          hashHistory.push('/app');
          dispatchNavigationEvent(`${sessionStorage.getItem('redirectTo')}`);
        }
      }).catch((error) => {
        this.state.loginError = true;
        this.setState(this.state);
        console.log("error ",error)
      })
    }
  }

  isLoggedIn = () => {
    let id = sessionStorage.getItem("app_id")
    let accesskey = sessionStorage.getItem("accessKey")
    let email = sessionStorage.getItem("email")
    if(!email || !id  || !accesskey) {
      return false
    }
    let ciphertext = CryptoJS.AES.decrypt(accesskey.toString(),email.toString()).toString(CryptoJS.enc.Utf8);
    console.log("id and ciphertext",id,ciphertext)
    if(id == ciphertext) {
      return true
    }
    return false
  }

  localLogin = (email_id,id) => {
    console.log("localLogin...",email_id,id)
    sessionStorage.setItem("email",email_id);
    sessionStorage.setItem("app_id",id);
    var ciphertext = CryptoJS.AES.encrypt(id.toString(),email_id);
    console.log("ciphertext is ",ciphertext.toString())
    sessionStorage.setItem("accessKey",ciphertext.toString());
    store.dispatch(initApp());
    hashHistory.push('/app');
    dispatchNavigationEvent(`${sessionStorage.getItem('redirectTo')}`);
  }

  googleLogin = (email_id,id,name,image_url) => {
      this.state.loginError = false;
      console.log("email name and imageUrl is ",image_url)
      axios.post(`auth/google`,{"email_id": email_id,"id": id,"name": name, "image_url": image_url}).then((response) => {
          console.log("google auth response ",response)
          console.log("type of id is ",typeof response.data.id)
          this.localLogin(email_id,response.data.id)
      }).catch((error) => {
        this.state.loginError = true;
        this.setState(this.state);
        console.log("google auth error ",error)
      })
  }

  facebookLogin = (email,id,name,url) => {
      this.state.loginError = false;
       console.log("email name and imageUrl is ",url,email,id,name);
      // console.log("email name and imageUrl is ",image_url)email_id"=> email,"user_id" => id, "name" => name, "image_url" => image_url
      axios.post(`auth/facebook`,{"email_id": email,"user_id": id,"name": name, "image_url": url}).then((response) => {
          console.log("facebook auth response ",response)
          console.log("google auth response ",response)
          console.log("type of id is ",typeof response.data.id)
          this.localLogin(email,response.data.id)
      }).catch((error) => {
        this.state.loginError = true;
        this.setState(this.state);
        console.log("facebook auth error ",error)
      })
  }

  doSignup = ()=> {
    console.log("do signup")
    console.log("refs...")
    this.state.empty_field_signup = false;
    this.state.terms_disagree = false;
    this.state.password_mismatch = false;
    this.state.invalid_signup_input = false;
    this.state.account_exists = false;
    this.state.loginError = false;
    let age = this.refs.signup_age.value;
    let location = this.refs.signup_location.value;
    let occupation = this.refs.signup_occupation.value;

    console.log("refs are ",this.refs)
    let email_id = this.refs.signup_email.value;
    console.log("email_id...",email_id)
    let password = this.refs.signup_password.value;
    console.log("password...",password)
    let confirm_password = this.refs.signup_conf_password.value;
    console.log("confirm_password...",confirm_password)
    let name = this.refs.signup_name.value;
    console.log("name...",name)
    var email_regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/ ;
    let empty_field = R.any((element) => {
      return (R.isNil(element) || R.isEmpty(element))
    },[name,email_id,password,confirm_password,age,location,occupation])
    if(empty_field) {
      console.log("here")
      this.state.empty_field_signup = true;
      this.setState(this.state);
    } else if (!this.state.checked) {
      this.state.terms_disagree = true;
      this.setState(this.state);
    }  else if(password != confirm_password) {
      this.state.password_mismatch = true;
      this.setState(this.state);
    } else if(!email_regex.test(email_id) || (password.length<5) || !(/^(0?[1-9]|[1-9][0-9])$/.test(age))) {
      this.state.invalid_signup_input = true;
      this.setState(this.state);
    } else {
      console.log("sending data to signup");
      axios.post(`/auth/signup/${email_id}`,{password: password,name: name,age: age,location: location,occupation: occupation}).then((response) => {
        console.log("success !",response);
        if(response.status == 200) {
          console.log("here....",typeof(response.data.id));
          this.localLogin(response.data.email_id.toString(),response.data.id.toString())
          store.dispatch(initApp());
          hashHistory.push('/app');
          dispatchNavigationEvent(`${sessionStorage.getItem('redirectTo')}`);
        } else if(response.status == 201) {
          this.state.account_exists = true;
          this.setState(this.state);
        }
      }).catch((error) => {
        console.log("error ",error)
      })
    }

  }


  render() {
    console.log("state is ",this.state)
   const responseGoogle = (response) => {
     console.log("getting response",response)
     console.log(response.profileObj);
     let name = response.profileObj.name;
     let email_id = response.profileObj.email;
     let image_url = response.profileObj.imageUrl;
     let id = response.profileObj.googleId;
     this.googleLogin(email_id,id,name,image_url)
   }

   const label = (
     <span style={{'display': 'inline-block'}}>I have read and agree to the&nbsp;
       <a
         href="/terms_and_conditions"
         target="_blank"
       >
         Terms and Conditions
       </a>
     </span>
   )
   var email_regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/ ;
   let abc = 0;
   return (
     <div className="coverPic" style={{'background-image': 'url("images/loginCover.png")',width: '100%',height:'100%',overflowY: 'auto'}}>
       {
         do {
           abc = this.state.sign ? 1 : 2
           if(this.state.signin) {
             <div  style={{'display': 'table','margin-left': '29%','margin-right': '29%',marginTop:'30px','width':'42%',background: '#FFFFFF'}}>
             <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center', 'padding-top': '20px','padding-bottom': '20px'}} >
                  <img src="images/zencil.png" />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'width': '100%','display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center'}} >
                  <div style={{display: 'flex',width:'50%',marginLeft: '25%', marginRight: '25%'}} >
                    <a onClick={() => ::this.onAction(1)} style = {{cursor:'pointer',paddingTop: '5px',paddingBottom: '5px',display:'inline-block',width:'50%',background: (this.state.signin ? "#0058C0" : "#FFFFFF") ,border: '1px solid #0057C2',borderRight: 'none',borderRadius: "4px 0px 0px 4px",fontFamily: 'Avenir-Book',fontSize: '12px',color: (this.state.signin ? "#FFFFFF" : "#0058C0"),letterSpacing: '0.6px'}}>Sign In </a>
                    <a onClick={() => ::this.onAction(-1)} style = {{cursor:'pointer',paddingTop: '5px',paddingBottom: '5px',display:'inline-block',background: (this.state.signin ? "#FFFFFF" : "#0058C0"),width:'50%',border: '1px solid #0057C2',borderLeft: 'none',borderRadius: "0px 4px 4px 0px",fontFamily: 'Avenir-Book',fontSize: '12px',color: (this.state.signin ? "#0058C0" : "#FFFFFF")  ,letterSpacing: '0.6px'}}>Sign Up </a>
                  </div>
                </div>
              </div>
              {
                do {
                  if(this.state.loginError) {
                    <div style={{'display': 'table-row',width:'100%'}}>
                      <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                        <div style={{fontSize: '14px',letterSpacing: '1px',paddingTop: '11px',color: 'red',paddingBottom: '11px','width': '60%' ,marginLeft: '20%', marginRight: '20%','height': '100%'}}> Invalid Credentials</div>
                      </div>
                    </div>
                  }
                }
              }
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': (this.state.loginError ? '25px' : '50px')}} >
                  <input type="text" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "EMAIL"} ref="login_email" placeholder="EMAIL" style={{fontSize: '14px',outline: 'none' ,letterSpacing: '1px',paddingTop: '11px',color: '#666666',paddingBottom: '11px',paddingLeft: '12px','width': '60%' ,border: "none" ,background: '#EBEBEB',borderRadius: '4px' ,'height': '100%'}} />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                  <input type="password" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "PASSWORD"} ref="login_password" placeholder="PASSWORD" style={{fontSize: '14px',outline: 'none' ,letterSpacing: '1px',paddingTop: '11px',color: '#666666',paddingBottom: '11px',paddingLeft: '12px','width': '60%' ,border: "none" ,background: '#EBEBEB',borderRadius: '4px' ,'height': '100%'}} />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '50px'}} >
                  <button onClick={::this.doLogin} style={{outline: 'none',paddingTop: '12px',paddingBottom: '12px',width:'67%',border:'none',color:'#FFFFFF',background: '#56A4FF','border-radius': '4px',fontFamily: 'Avenir-Medium',fontSize: '16px',color: '#FFFFFF',letterSpacing: '1.14px'}} >SIGN IN</button>
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '50px'}} >
                  <div style={{color: "#666666",fontFamily: 'Avenir-Medium',fontSize: '16px',letterSpacing: '1.14px'}} >OR</div>
                </div>
              </div>
              <div style={{cursor: 'pointer','display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '40px',paddingBottom: '25px'}} >
                  <a onClick={(ev) => ::this.handleClick()} > <img src="images/bitmap.png" /> </a>
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{cursor: 'pointer','display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center',paddingBottom: '25px'}} >
                  <GoogleLogin style={{'background' : '#FFFFFF',border: 'none', outline: 'none'}}
                  clientId="1069828445362-l9fvsjiils7fm4n1mjvtgt96ib9kif68.apps.googleusercontent.com"
                  onSuccess={responseGoogle}
                  onFailure={responseGoogle}> <img src="images/google.png" />
                  </GoogleLogin>
                </div>
              </div>
             </div>
           } else {
             <div  style={{'display': 'table',marginTop:'30px','margin-left': '29%','margin-right': '29%','width':'42%',background: '#FFFFFF'}}>
             <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center', 'padding-top': '20px','padding-bottom': '20px'}} >
                  <img src="images/zencil.png" />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'width': '100%','display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center'}} >
                  <div style={{display: 'flex',width:'50%',marginLeft: '25%', marginRight: '25%'}} >
                    <a onClick={() => ::this.onAction(1)} style = {{cursor:'pointer',paddingTop: '5px',paddingBottom: '5px',display:'inline-block',width:'50%',background: (this.state.signin ? "#0058C0" : "#FFFFFF") ,border: '1px solid #0057C2',borderRight: 'none',borderRadius: "4px 0px 0px 4px",fontFamily: 'Avenir-Book',fontSize: '12px',color: (this.state.signin ? "#FFFFFF" : "#0058C0"),letterSpacing: '0.6px'}}>Sign In </a>
                    <a onClick={() => ::this.onAction(-1)} style = {{cursor:'pointer',paddingTop: '5px',paddingBottom: '5px',display:'inline-block',background: (this.state.signin ? "#FFFFFF" : "#0058C0"),width:'50%',border: '1px solid #0057C2',borderLeft: 'none',borderRadius: "0px 4px 4px 0px",fontFamily: 'Avenir-Book',fontSize: '12px',color: (this.state.signin ? "#0058C0" : "#FFFFFF")  ,letterSpacing: '0.6px'}}>Sign Up </a>
                  </div>
                </div>
              </div>
              {
                  do {
                      if(this.state.empty_field_signup) {
                        <div style={{'display': 'table-row',width:'100%'}}>
                          <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                            <div style={{fontSize: '14px',letterSpacing: '1px',paddingTop: '11px',color: 'red',paddingBottom: '11px','width': '60%' ,marginLeft: '20%', marginRight: '20%','height': '100%'}}> Fields cannot be left empty</div>
                          </div>
                        </div>
                      } else if(this.state.terms_disagree) {
                        <div style={{'display': 'table-row',width:'100%'}}>
                          <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                            <div style={{fontSize: '14px',letterSpacing: '1px',paddingTop: '11px',color: 'red',paddingBottom: '11px','width': '60%' ,marginLeft: '20%', marginRight: '20%','height': '100%'}}> Please agree to the terms and conditions before continuing</div>
                          </div>
                        </div>
                      } else if(this.state.password_mismatch) {
                        <div style={{'display': 'table-row',width:'100%'}}>
                          <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                            <div style={{fontSize: '14px',letterSpacing: '1px',paddingTop: '11px',color: 'red',paddingBottom: '11px','width': '60%' ,marginLeft: '20%', marginRight: '20%','height': '100%'}}> Password and Confirm password fields do not match</div>
                          </div>
                        </div>
                      } else if(this.state.invalid_signup_input) {
                        <div style={{'display': 'table-row',width:'100%'}}>
                          <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                            <div style={{fontSize: '14px',letterSpacing: '1px',paddingTop: '11px',color: 'red',paddingBottom: '11px','width': '60%' ,marginLeft: '20%', marginRight: '20%','height': '100%'}}> {!email_regex.test(this.refs.signup_email.value) ? "Invalid Email Id" : (/^(0?[1-9]|[1-9][0-9])$/.test(this.refs.signup_age.value) ? "Password field must have atleast 5 characters" : "Invalid Age input")}</div>
                          </div>
                        </div>
                      } else if(this.state.account_exists) {
                        <div style={{'display': 'table-row',width:'100%'}}>
                          <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                            <div style={{fontSize: '14px',letterSpacing: '1px',paddingTop: '11px',color: 'red',paddingBottom: '11px','width': '60%' ,marginLeft: '20%', marginRight: '20%','height': '100%'}}> {this.refs.signup_email.value} is already registered with Zencil. Please reset your password or Login with Google+</div>
                          </div>
                        </div>
                      }
                  }
              }
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                  <input type="text" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "NAME"} ref="signup_name" placeholder="NAME" style={{fontSize: '14px',outline: 'none' ,letterSpacing: '1px',paddingTop: '11px',color: '#666666',paddingBottom: '11px',paddingLeft: '12px','width': '60%' ,border: "none" ,background: '#EBEBEB',borderRadius: '4px' ,'height': '100%'}} />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                  <input type="text" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "EMAIL"} ref="signup_email" placeholder="EMAIL" style={{fontSize: '14px',outline: 'none' ,letterSpacing: '1px',paddingTop: '11px',color: '#666666',paddingBottom: '11px',paddingLeft: '12px','width': '60%' ,border: "none" ,background: '#EBEBEB',borderRadius: '4px' ,'height': '100%'}} />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                  <input type="password" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "PASSWORD"} ref="signup_password" placeholder="PASSWORD" style={{fontSize: '14px',outline: 'none' ,letterSpacing: '1px',paddingTop: '11px',color: '#666666',paddingBottom: '11px',paddingLeft: '12px','width': '60%' ,border: "none" ,background: '#EBEBEB',borderRadius: '4px' ,'height': '100%'}} />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                  <input type="password" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "CONFIRM PASSWORD"} ref="signup_conf_password" placeholder="CONFIRM PASSWORD" style={{fontSize: '14px',outline: 'none' ,letterSpacing: '1px',paddingTop: '11px',color: '#666666',paddingBottom: '11px',paddingLeft: '12px','width': '60%' ,border: "none" ,background: '#EBEBEB',borderRadius: '4px' ,'height': '100%'}} />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                  <input type="text" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "AGE"} ref="signup_age" placeholder="AGE" style={{fontSize: '14px',outline: 'none' ,letterSpacing: '1px',paddingTop: '11px',color: '#666666',paddingBottom: '11px',paddingLeft: '12px','width': '15%' ,border: "none" ,background: '#EBEBEB',borderRadius: '4px' ,'height': '100%'}} />
                  <input type="text" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "OCCUPATION"} ref="signup_occupation" placeholder="OCCUPATION" style={{fontSize: '14px',outline: 'none' ,letterSpacing: '1px',paddingTop: '11px',color: '#666666',paddingBottom: '11px',paddingLeft: '12px','width': '35%' ,marginLeft: '10%',border: "none" ,background: '#EBEBEB',borderRadius: '4px' ,'height': '100%'}} />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                  <input type="text" onFocus={(e) => e.target.placeholder = ""} onBlur={(e) => e.target.placeholder = "LOCATION"} ref="signup_location" placeholder="LOCATION" style={{fontSize: '14px',outline: 'none' ,letterSpacing: '1px',paddingTop: '11px',color: '#666666',paddingBottom: '11px',paddingLeft: '12px','width': '60%' ,border: "none" ,background: '#EBEBEB',borderRadius: '4px' ,'height': '100%'}} />
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px'}} >
                <MuiThemeProvider>
                <label style={{width: '60%',marginLeft: '20%', marginRight: '20%',display: 'flex'}}>
                  <Checkbox   iconStyle={{fill: '#56A4FF'}}
 style={{width: '10%', color: '#56A4FF'}} onCheck = {::this.onCheck} />
                  {label}
                </label>
                </MuiThemeProvider>
                </div>
              </div>
              <div style={{'display': 'table-row',width:'100%'}}>
                <div style={{'display' : 'table-cell','verticalAlign': 'middle','textAlign': 'center','paddingTop': '25px',paddingBottom: '30px'}} >
                  <button onClick={::this.doSignup} style={{outline: 'none',paddingTop: '12px',paddingBottom: '12px',width:'67%',border:'none',color: "#FFFFFF",background: '#56A4FF','border-radius': '4px',fontFamily: 'Avenir-Medium',fontSize: '16px',color: '#FFFFFF',letterSpacing: '1.14px'}} >SIGN UP</button>
                </div>
              </div>
             </div>
           }
         }
       }
     </div>
   )
 }
}

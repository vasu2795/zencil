import React from 'react';
import R from 'ramda';

import Navbar from './navigation/Navbar';
import Footer from './Footer';
import {hashHistory} from 'react-router'
import CryptoJS from 'crypto-js'
import dispatchNavigationEvent from '../RoutingManagement';

export default class MainApp extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      sortBy: "Latest",
      didMount: false
    }
  }

  onSelect = (sortBy) => {
    console.log("here... mainapp",sortBy)
    this.state.sortBy = sortBy;
    this.setState(this.state);
  }
  componentWillMount() {
    console.log("will mount")
  }
  componentDidMount() {
    console.log("did mount");
  }

  isLoggedIn = () => {
    console.log("islogged in ",sessionStorage.getItem("email"),sessionStorage.getItem("accessKey"))
    let id = sessionStorage.getItem("app_id");
    let accesskey = sessionStorage.getItem("accessKey")
    let email = sessionStorage.getItem("email")
    if(!(email && id  && accesskey)) {
      return false
    }
    let ciphertext = CryptoJS.AES.decrypt(sessionStorage.getItem("accessKey"),sessionStorage.getItem("email")).toString(CryptoJS.enc.Utf8);
    let plain = ciphertext.toString(CryptoJS.enc.Utf8)
    console.log("id and ciphertext",id,ciphertext,plain)
    if(id == plain) {
      return true
    }
    return false
  }

  componentWillReceiveProps(props) {
    console.log("Receiving props",props)
    if(!this.state.didMount) {
      let currentUrl = window.location.hash.split("/#").join('').split("#").join('');
      if(!this.isLoggedIn()) {
        console.log("mounting again")
        sessionStorage.setItem('redirectTo',currentUrl)
        dispatchNavigationEvent('/login')
      }
    }
  }

  render() {
    console.log("children here....",this.state)
    return (
      <div style={{'height': '100%','min-width': '1024px',overflow: 'auto'}}>
        <Navbar onSelect = {this.onSelect}/>
        <div className="body_zencil" style={{
          'overflow': 'auto', 'min-width': '1024px'
        }}>
        {React.Children.map(this.props.children, child => React.cloneElement(child, {sortBy: this.state.sortBy}))}
        </div>
      </div>
    )
  }
}

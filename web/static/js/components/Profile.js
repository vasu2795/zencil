import React from 'react';
import Avatar from 'react-avatar';
import moment from 'moment';
import dispatchNavigationEvent from '../RoutingManagement';
import ReactRectangle from 'react-rectangle';
import axios from 'axios';
import R from 'ramda';
import {Tabs, Tab} from 'material-ui/Tabs';
// From https://github.com/oliviertassinari/react-swipeable-views
import SwipeableViews from 'react-swipeable-views';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import CryptoJS from 'crypto-js'
import GoogleLogin from 'react-google-login';


import {
  ShareButtons,
  ShareCounts,
  generateShareIcon
} from 'react-share';
const {
  FacebookShareButton,
  GooglePlusShareButton,
  TwitterShareButton,
  } = ShareButtons;
const {
  FacebookShareCount,
  GooglePlusShareCount,
} = ShareCounts;

const FacebookIcon = generateShareIcon('facebook');
const GooglePlusIcon = generateShareIcon('google');
const TwitterIcon = generateShareIcon('twitter');
const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  slide: {
    padding: 10,
  },
};


class MyAccount extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      age: null,
      email_id: null,
      id: null,
      location: null,
      occupation : null,
      username :null,
      description: null,
      advanced_id: null,
      image_url: null,
      location: null,
      occupation: null,
      role: null,
      signup_method: null,
      updateStatus: null,
      old_password: null,
      new_password: null,
      confirm_passoword: null,
      focus: -1
    }
  }

  componentDidMount() {
    this.setInitialState();
  }

  componentWillReceiveProps(newProps){
    if(this.props != newProps) {
      this.props = newProps;
      this.setInitialState();
    }
  }

  setInitialState = () => {
    console.log("props to set initial state is ",this.props);
    let that = this.props.profile;
    for (var k in this.state)
      this.state[k] = that[k];
    this.setState(this.state);
    console.log("state set ... ",this.state);
  }

  updateDetails = (ev) => {
    // check if value has changed
    let flag = 1;
    ev.nativeEvent.preventDefault();
    console.log("handling form submit....")
    this.state.updateStatus = 'loading';
    this.setState(this.state);
    console.log("after form submit")
    for(var k in this.state) {
      if(['age','username','image_url','location','occupation'].indexOf(k) > -1) {
        console.log("k before ",k)
        if(this.state[k] != this.props.profile[k]) {
          flag = 1;
          let promise = axios.post(`profile/${this.state.id}/update`,{"age": this.state.age,"username": this.state.username,"location": this.state.location,"occupation": this.state.occupation})
          promise.then((response) => {
            this.state.updateStatus = 'success'
            this.setState(this.state);
          }).catch((error) => {
            this.state.updateStatus = 'updateFailed';
            this.setState(this.state);
          });
          break;
        }
      }
    }
    if(flag) {
      this.state.updateStatus = 'success';
      this.setState(this.state);
    }
  }

  updatePassword = (ev) => {
    // check if value has changed
    let flag = 1;
    ev.nativeEvent.preventDefault();
    console.log("handling form submit....")
    this.state.updateStatus = 'loading';
    console.log("after form submit",this.state)
    if(!this.state.new_password || !this.state.old_password || !this.state.confirm_password) {
      this.state.updateStatus = 'password_reset_empty_field';
    } else if (this.state.new_password != this.state.confirm_password) {
      console.log("unequal ? ",this.state.new_password, this.state.confirm_password, this.state.new_password == this.state.confirm_password);
      this.state.updateStatus = 'password_reset_mismatch';
    } else if(this.state.new_password.length<5) {
      this.state.updateStatus = 'password_reset_strength';
    }else {
      let promise = axios.post(`profile/${this.props.profile.email_id}/password_update`,{"old_password": this.state.old_password,"new_password": this.state.new_password})
      promise.then((response) => {
        if(response.status == 201) {
          this.state.updateStatus = 'password_reset_failure'
        } else if(response.status == 200) {
          this.state.updateStatus = 'password_reset_success'
        }
        this.setState(this.state);
      }).catch((error) => {
        console.log("error is ",error);
        this.state.updateStatus = 'password_reset_failed';
        this.setState(this.state);
      });
    }
    this.setState(this.state);
  }


  bindData = (path, event) => {
      console.log("binding data ..... ");
      let value = event.nativeEvent.target.value;
      this.state[path] = value;
      console.log("state after binding...",this.state);
      this.setState(this.state);
    };


  render() {
    const responseGoogle = (response) => {
     console.log("getting response",response)
     console.log(response.profileObj);
     let name = response.profileObj.name;
     let email_id = response.profileObj.email;
     let image_url = response.profileObj.imageUrl;
     console.log("image url is ",image_url)
    }

    return(
      <div>
        <div style={{fontFamily: 'Avenir-Light',fontSize: '16px',color: this.state.focus>-1 ?'rgb(86, 164, 255)': '#999999',letterSpacing: '0.8px',marginTop: '25px'}}> Profile Details </div>
        <div className="ui visible divider" />
        <form  onSubmit={(ev) => ::this.updateDetails(ev)}>
            {do {
               if(this.state.updateStatus === 'updateFailed') {
                 <div className="ui visible error message">
                   <div className="header">Failed to Update</div>
                   Failed to update details. Please try again.
                 </div>
               } else if(this.state.updateStatus === 'invalidInput') {
                 <div className="ui visible error message">
                   <div className="header">Failed to Update</div>
                   Invalid input. Please try again.
                 </div>
               } else if(this.state.updateStatus === 'success') {
                 <div className="ui visible success message">
                   <div className="header">Update Success</div>
                   Profile Details have been updated. <a onClick={(ev) => { dispatchNavigationEvent(`profile/${this.state.id}/data`);} }> Refresh </a> the page to view your updated profile.
                 </div>
               }
             }}
             <div style = {{paddingTop: '20px','width': '100%','display': 'flex'}} className="field">
               <label style={{width: '30%','font-family': 'Avenir-Light','font-size': '16px',color: (this.state.focus == 0 ? 'rgb(86, 164, 255)' : 'rgb(153, 153, 153)'),'letter-spacing': '0.8px','white-space': 'nowrap',float: 'left',padding: '0.67861429em 1em'}}>Profile Name</label>
               <input style={{width: '70%',float: 'right', paddingLeft: '10px', fontFamily: 'Avenir-Book'}}
                   required
                   value={this.state.username}
                   onFocus={() => {this.state.focus = 0; this.setState(this.state);}}
                   onBlur={() => {this.state.focus = -1; this.setState(this.state);}}
                   onChange={ev => ::this.bindData('username', ev)} />
             </div>
             <div style = {{paddingTop: '20px','width': '100%','display': 'flex'}} className="field">
              <label style={{width: '30%','font-family': 'Avenir-Light','font-size': '16px',color: 'rgb(153, 153, 153)','letter-spacing': '0.8px','white-space': 'nowrap',float: 'left',padding: '0.67861429em 1em'}}>Email Id</label>
              <input style={{width: '70%',float: 'right', paddingLeft: '10px', fontFamily: 'Avenir-Book'}} disabled="true"
                  required
                  value={this.state.email_id}
              />
            </div>
            <div style = {{paddingTop: '20px','width': '100%','display': 'flex'}} className="field">
              <label style={{width: '30%','font-family': 'Avenir-Light','font-size': '16px',color: (this.state.focus == 1 ? 'rgb(86, 164, 255)' : 'rgb(153, 153, 153)'),'letter-spacing': '0.8px','white-space': 'nowrap',float: 'left',padding: '0.67861429em 1em'}}>Age</label>
              <input style={{width: '70%',float: 'right', paddingLeft: '10px', fontFamily: 'Avenir-Book'}}
                  value={this.state.age}
                  onFocus={() => {this.state.focus = 1; this.setState(this.state);}}
                  onBlur={() => {this.state.focus = -1; this.setState(this.state);}}
                  ref = "age"
                  onChange={ev => ::this.bindData('age', ev)}/>
            </div>
            <div style = {{paddingTop: '20px','width': '100%','display': 'flex'}} className="field">
              <label style={{width: '30%','font-family': 'Avenir-Light','font-size': '16px',color: (this.state.focus == 2 ? 'rgb(86, 164, 255)' : 'rgb(153, 153, 153)'),'letter-spacing': '0.8px','white-space': 'nowrap',float: 'left',padding: '0.67861429em 1em'}}>Location</label>
              <input style={{width: '70%',float: 'right', paddingLeft: '10px', fontFamily: 'Avenir-Book'}}
                  value={this.state.location}
                  ref = "location"
                  onFocus={() => {this.state.focus = 2; this.setState(this.state);}}
                  onBlur={() => {this.state.focus = -1; this.setState(this.state);}}
                  onChange={ev => ::this.bindData('location', ev)}/>
            </div>
            <div style = {{paddingTop: '20px','width': '100%','display': 'flex'}} className="field">
              <label style={{width: '30%','font-family': 'Avenir-Light','font-size': '16px',color: (this.state.focus == 3 ? 'rgb(86, 164, 255)' : 'rgb(153, 153, 153)'),'letter-spacing': '0.8px','white-space': 'nowrap',float: 'left',padding: '0.67861429em 1em'}}>Occupation</label>
              <input style={{width: '70%',float: 'right', paddingLeft: '10px', fontFamily: 'Avenir-Book'}}
                  value={this.state.occupation}
                  ref = "occupation"
                  onFocus={() => {this.state.focus = 3; this.setState(this.state);}}
                  onBlur={() => {this.state.focus = -1; this.setState(this.state);}}
                  onChange={ev => ::this.bindData('occupation', ev)}/>
            </div>
            <div style={{display: 'flex',justifyContent: 'center'}} >
              <button style={{marginTop: '30px',background: 'rgb(86, 164, 255)',color: '#FFFFFF'}} className={"ui button" + (this.state.updateStatus === 'loading' ? ' loading' : '')}>Update Details</button>
            </div>
          </form>
          <div style={{fontFamily: 'Avenir-Light',fontSize: '16px',color: '#999999',letterSpacing: '0.8px',marginTop: '30px'}}> Reset Password</div>
          <div className="ui visible divider" />
          <form  onSubmit={(ev) => ::this.updatePassword(ev)}>
              {do {
                 if(this.state.updateStatus === 'password_reset_failed') {
                   <div className="ui visible error message">
                     <div className="header">Failed to Update</div>
                     Failed to update details. Please try again.
                   </div>
                 } else if(this.state.updateStatus === 'password_reset_empty_field') {
                   <div className="ui visible error message">
                     <div className="header">Failed to Update</div>
                     Feilds cannot be empty.
                   </div>
                 } else if(this.state.updateStatus === 'password_reset_mismatch') {
                   <div className="ui visible error message">
                     <div className="header">Password Mismatch</div>
                     New password and confirm password do not match.
                   </div>
                 } else if(this.state.updateStatus === 'password_reset_success') {
                   <div className="ui visible success message">
                     <div className="header">Update Success</div>
                     Password has been updated !
                   </div>
                 } else if(this.state.updateStatus == 'password_reset_strength') {
                   <div className="ui visible error message">
                     <div className="header">Failed to </div>
                     Password should have atleast 5 characters
                   </div>
                 } else if(this.state.updateStatus == 'password_reset_failure') {
                   <div className="ui visible error message">
                     <div className="header">Failed to </div>
                     Password update failed. Please try again.
                   </div>
                 }
               }}
               <div style = {{paddingTop: '20px','width': '100%','display': 'flex'}} className="field">
                 <label style={{width: '30%','font-family': 'Avenir-Light','font-size': '16px',color: (this.state.focus == 4 ? 'rgb(86, 164, 255)' : 'rgb(153, 153, 153)'),'letter-spacing': '0.8px','white-space': 'nowrap',float: 'left',padding: '0.67861429em 1em'}}>Old Password</label>
                 <input style={{width: '70%',float: 'right', paddingLeft: '10px', fontFamily: 'Avenir-Book'}}
                     required
                     value={this.state.old_password}
                     onFocus={() => {this.state.focus = 4; this.setState(this.state);}}
                     onBlur={() => {this.state.focus = -1; this.setState(this.state);}}
                     onChange={ev => ::this.bindData('old_password', ev)} />
               </div>
               <div style = {{paddingTop: '20px','width': '100%','display': 'flex'}} className="field">
                <label style={{width: '30%','font-family': 'Avenir-Light','font-size': '16px',color: (this.state.focus == 1 ? 'rgb(86, 164, 255)' : 'rgb(153, 153, 153)'),'letter-spacing': '0.8px','white-space': 'nowrap',float: 'left',padding: '0.67861429em 1em'}}>New Password</label>
                <input style={{width: '70%',float: 'right', paddingLeft: '10px', fontFamily: 'Avenir-Book'}}
                    value={this.state.new_password}
                    onFocus={() => {this.state.focus = 5; this.setState(this.state);}}
                    onBlur={() => {this.state.focus = -1; this.setState(this.state);}}
                    ref = "age"
                    onChange={ev => ::this.bindData('new_password', ev)}/>
              </div>
              <div style = {{paddingTop: '20px','width': '100%','display': 'flex'}} className="field">
                <label style={{width: '30%','font-family': 'Avenir-Light','font-size': '16px',color: (this.state.focus == 2 ? 'rgb(86, 164, 255)' : 'rgb(153, 153, 153)'),'letter-spacing': '0.8px','white-space': 'nowrap',float: 'left',padding: '0.67861429em 1em'}}>Confirm Password</label>
                <input style={{width: '70%',float: 'right', paddingLeft: '10px', fontFamily: 'Avenir-Book'}}
                    value={this.state.confirm_password}
                    ref = "location"
                    onFocus={() => {this.state.focus = 6; this.setState(this.state);}}
                    onBlur={() => {this.state.focus = -1; this.setState(this.state);}}
                    onChange={ev => ::this.bindData('confirm_password', ev)}/>
              </div>
              <div style={{display: 'flex',justifyContent: 'center'}} >
                <button style={{marginTop: '30px',background: 'rgb(86, 164, 255)',color: '#FFFFFF'}} className={"ui button" + (this.state.updateStatus === 'loading' ? ' loading' : '')}>Update Password</button>
              </div>
            </form>
         </div>
    );
  }

}

class Leftbar extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      followers: 0,
      following: 0,
      posts: 0,
      profile: null
    }
  }

  componentDidMount() {
    console.log("props is ",this.props)
      this.state.followers = this.props.followers.length;
      this.state.following = this.props.following.length;
      this.state.profile = this.props.profile;
      this.state.posts = this.props.posts ? this.props.posts.length : 0;
      this.setState(this.state);
  }

  componentWillReceiveProps(newProps) {
    console.log("component received props ?",newProps,this.props);
    if(this.props != newProps) {
      this.state.followers = newProps.followers.length;
      this.state.following = newProps.following.length;
      this.state.profile = newProps.profile;
      this.state.posts = newProps.posts ? newProps.posts.length : 0;
      this.setState(this.state);
    }
  }

  render(){
    console.log("leftbar state ",this.state)
    let image_url = (this.state.profile && this.state.profile.facebookimageurl) ? this.state.profile.facebookimageurl : ( this.state.profile && this.state.profile.googleimgurl) ? this.state.profile.googleimgurl : "images/user.png"
    console.log("image url ",image_url)
    return(
        <div style={{display: 'inline-block',width: '36.13%',height: '100%'}} >
          <div style={{marginLeft: '40%',marginRight: '15.67%',width: '43.33%'}}>
            <ReactRectangle>
              <Avatar round = 'true' size="100%" facebook-id="invalidfacebookusername" src={image_url} />
            </ReactRectangle>
          </div>
          <div style={{display: 'table',paddingLeft: '40%',paddingRight: '15.67%',marginTop: '10%',width: '100%',height: '40%'}}>
            <div style={{display:'table-row', margin: 'auto'}}>
              <div style={{display:'table-cell',verticalAlign: 'top',fontFamily: 'Avenir-Light',fontSize: '16px',color: '#999999',letterSpacing: '0.8px'}}>Followers </div>
              <div style={{display:'table-cell'}} />
              <div style={{display:'table-cell',fontFamily: 'Avenir-Book',fontSize: '32px',color: '#56A4FF',letterSpacing: '1.8px'}}>{this.state.followers}</div>
            </div>
            <div style={{display:'table-row'}}>
              <div style={{display:'table-cell',verticalAlign: 'top',fontFamily: 'Avenir-Light',fontSize: '16px',color: '#999999',letterSpacing: '0.8px'}}>Following </div>
              <div style={{display:'table-cell'}} />
              <div style={{display:'table-cell',fontFamily: 'Avenir-Book',fontSize: '32px',color: '#56A4FF',letterSpacing: '1.8px'}}>{this.state.following}</div>
            </div>
            <div style={{display:'table-row'}}>
              <div style={{display:'table-cell',verticalAlign: 'top',fontFamily: 'Avenir-Light',fontSize: '16px',color: '#999999',letterSpacing: '0.8px'}}>Posts </div>
              <div style={{display:'table-cell'}} />
              <div style={{display:'table-cell',fontFamily: 'Avenir-Book',fontSize: '32px',color: '#56A4FF',letterSpacing: '1.8px'}}>{this.state.posts}</div>
            </div>
            <div style={{display:'table-row',margin: 'auto'}} />
          </div>
        </div>
    );
  }
}

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
    }
  }
  componentDidMount() {
  }

  componentWillReceiveProps(newProps){
  }

  render() {
    return(
      <div onClick={() => {dispatchNavigationEvent(`post/3/view`)}} style={{cursor: 'pointer',display: 'table-cell',width:'47.5%',paddingBottom: '20px'}}>
        <div className="coverPic" style={{display: 'table',width:'100%',height: '150px',backgroundImage: 'url("../images/test.png")'}}>
        </div>
        <div style={{display:'table',height:'75px',width: '100%',verticalAlign:'middle'}}>
          <div style={{paddingLeft: '5%',paddingRight: '5%',width:'100%',display: 'table-cell',verticalAlign:'middle'}}>
            <div style={{'display': 'table','table-layout': 'fixed',width: '100%'}} >
              <div  style={{textAlign: 'justify',display: 'table-cell',overflow: 'hidden','word-break': 'break-all','word-wrap': 'break-word',textOverflow: 'ellipsis',whiteSpace: 'nowrap', fontFamily: 'Avenir-Medium',fontSize: '12px',color: '#999999',letterSpacing: '1px'}}>LORUM IMPSUM CARATTA MAGNUM ROBOTO</div>
            </div>
            <div  style={{paddingTop: '10px',fontFamily: 'Avenir-Medium',fontSize: '12px',color: '#999999',letterSpacing: '1px'}}>
              <div style={{float: 'left'}} >July 27, 2017</div>
              <div style={{float: 'right',display:'flex'}} >
                <img src="images/hearts.png" />
                <div style={{display:'inline-block', marginLeft: '5px'}}>45</div>
              </div>
              <div style={{'display': 'table','table-layout': 'fixed',width:'50%','position': 'relative', 'margin': 'auto'}}>
                <div  style={{textAlign: 'center',display: 'table-cell',overflow: 'hidden','word-break': 'break-all','word-wrap': 'break-word',textOverflow: 'ellipsis',whiteSpace: 'nowrap', fontFamily: 'Avenir-Medium',fontSize: '12px',color: '#999999',letterSpacing: '1px'}}>Rhodes</div>
             </div>
           </div>
          </div>
        </div>
      </div>

    );
  }
}
class PostList extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      posts: this.props.posts
    }
  }

  componentDidMount() {
  }

  render() {
    console.log("this state ",this.state);
    this.state.posts = [1,2,3,4,5,6];
    let groupedPosts = R.groupWith((a,b)=> R.indexOf(a,this.state.posts) + 1 == R.indexOf(b,this.state.posts),this.state.posts)
    console.log("groupedPosts ",groupedPosts);

    return (
      <div style={{display:'table', marginTop: '40px',width: '100%'}}>
      {
        R.map(posts => {
          console.log("here... ",posts)
                if(posts[1]){
                  return(

                    <div style={{display: 'table-row', width: '100%'}}>
                      <Card post= {posts[0]}/>
                      <div style={{display: 'table-cell', width: '5%'}}>  </div>
                      <Card post={posts[1]}/>
                    </div>
                  );
                  } else {
                    return(

                      <div style={{display: 'table-row', width: '100%'}}>
                        <Card post= {posts[0]}/>
                        <div style={{display: 'table-cell', width: '52.5%'}}>  </div>
                      </div>
                    );
                }
        },groupedPosts)
      }
      </div>
    );
  }
}


class PeopleList extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      followers: [],
      following: [],
      odd: [],
      even: [],
      ready: false
    }
  }

  componentDidMount() {
    console.log("people mounted",this.props)
    this.setInitialState();
      }

  setInitialState = () => {
    this.state.odd = [];
    this.state.even = [];
    this.state.ready = false;
    if(this.props.following) {
      this.state.following = this.props.following
    }
     if(this.props.followers) {
      this.state.followers = this.props.followers
    }
    if(this.state.followers.length && this.state.following.length) {
      this.state.odd = this.state.followers;
      this.state.even = this.state.following;
    }
    else if(this.state.followers.length) {
      var i =0;
      R.forEach((profile) =>  {  console.log("i is ",i,i%2==0);
                                    if(i%2 == 0) {
                                      this.state.odd.push(profile)
                                    }
                                    else {
                                      this.state.even.push(profile)
                                    }
                                     i++;
                                  },this.state.followers)
    } else {
      var j =0;
      R.forEach((profile) =>  {  if(j%2 == 0)
                                      this.state.odd.push(profile)
                                    else
                                      this.state.even.push(profile)
                                     j++;
                                  },this.state.following)
    }
    this.state.ready = true;
    this.setState(this.state);
  }
  componentWillReceiveProps(newProps) {
    if(this.props!=newProps) {
      console.log("people received props different")

      this.props = newProps;
      this.setInitialState();
    }
  }

  render() {
    console.log("this people ",this.state);
    if(this.state.ready) {
      console.log("this stat followers",this.state.followers.length ? this.state.followers : this.state.following)
      return(
        <div>
        {
          do {
            if(this.state.followers.length && this.state.following.length) {
              <div style={{marginTop: '25px'}}>
                <div style={{width: '45%', float: 'left'}}>
                  <div style = {{position:'relative',height: '8.15%',width:'100%',borderBottom: '1px solid #D5D5D5', fontFamily: 'Avenir-Medium',fontSize: '16px',color: '#999999',letterSpacing: '0.7px'}} >FOLLOWERS</div>
                </div>
                <div style={{width: '45%', float: 'right'}}>
                  <div style = {{position:'relative',height: '8.15%',width:'100%',borderBottom: '1px solid #D5D5D5', fontFamily: 'Avenir-Medium',fontSize: '16px',color: '#999999',letterSpacing: '0.7px'}} >FOLLOWING</div>
                </div>
              </div>
            }
          }
        }
        <div style={{paddingTop: '30px',width:'100%',overflowY:'auto' , display: 'flex', flexWrap: 'wrap',justifyContent: 'space-around'}}>
        <div style={{float: 'left', width: '45%'}}>

        {R.map((people) => {
          return(
            <div onClick= {() => dispatchNavigationEvent(`profile/${people.id}/data`)} style={{cursor: 'pointer',minHeight: '50px',display: 'table',paddingBottom: '30px'}}>
              <div style={{display: 'table-cell',verticalAlign: 'middle'}}>
                <Avatar round="true" size="40" facebook-id="invalidfacebookusername" src="images/test.png" />
              </div>
              <div style = {{display: 'table-cell',verticalAlign: 'middle',paddingLeft: '10%'}}>
                <div style={{overflow: 'hidden','display': 'table','table-layout': 'fixed',width: '100%'}} >
                  <div  style={{textAlign: 'justify',display: 'table-cell',overflow: 'hidden','word-break': 'break-all','word-wrap': 'break-word',textOverflow: 'ellipsis',whiteSpace: 'nowrap', fontFamily: 'Avenir-Roman',fontSize: '14px',color: '#666666',letterSpacing: '0'}}>{people.username}</div>
                </div>
                <div style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden','textOverflow': 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '12px',color: '#666666',letterSpacing: '0'}} > {people.age ? (people.age + (people.occupation ? ("," + people.occupation) : "")) : (people.occupation ? people.occupation : (people.location ? people.location : people.email_id)) }</div>
              </div>
            </div>
          );
        },this.state.odd)}
      </div>

        <div style={{float: 'right', width: '45%'}}>

        {R.map((people) => {
          return(
            <div onClick= {() => dispatchNavigationEvent(`profile/${people.id}/data`)} style={{cursor: 'pointer',minHeight: '50px', display: 'table',paddingBottom: '30px'}}>
              <div style={{display: 'table-cell',verticalAlign: 'middle'}}>
                <Avatar round="true" size="40" facebook-id="invalidfacebookusername" src="images/test.png" />
              </div>
              <div style = {{display: 'table-cell',verticalAlign: 'middle',paddingLeft: '10%'}}>
                <div style={{overflow: 'hidden','display': 'table','table-layout': 'fixed',width: '100%'}} >
                  <div  style={{textAlign: 'justify',display: 'table-cell',overflow: 'hidden','word-break': 'break-all','word-wrap': 'break-word',textOverflow: 'ellipsis',whiteSpace: 'nowrap', fontFamily: 'Avenir-Roman',fontSize: '14px',color: '#666666',letterSpacing: '0'}}>{people.username}</div>
                </div>
                <div  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden','textOverflow': 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '12px',color: '#666666',letterSpacing: '0'}} > {people.age ? (people.age + (people.occupation ? ("," + people.occupation) : "")) : (people.occupation ? people.occupation : (people.location ? people.location : people.email_id)) }</div>
              </div>
            </div>
          );
        },this.state.even)}
      </div>
        </div>
        </div>
      );
    } else {
      return (
        <div> People </div>
      );
    }
  }
}

class FollowersList extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      followers: []
    }
  }

  componentDidMount() {
    console.log("people mounted",this.props)
    if(this.props.followers) {
      this.state.followers = this.props.followers
    }
    this.setState(this.state);
  }

  componentWillReceiveProps(newProps) {
    console.log("people received props",this.props)
    if(this.props!=newProps) {
      console.log("people received props different")
      this.props = newProps;
      if(this.props.followers) {
        this.state.followers = this.props.followers
      }
      this.setState(this.state);
    }
  }

  render() {
    console.log("this people ",this.state);
    return(
      <div style={{display: 'flex',width:'100%',overflowY:'auto'}}>
      {R.map((people) => {
        return(
          <div onClick= {() => dispatchNavigationEvent(`profile/${people.id}/data`)} style={{cursor:'pointer',minHeight: '50px', height: '8.15%',display: 'table'}}>
            <div style={{display: 'table-cell',verticalAlign: 'middle'}}>
              <Avatar size="30" round='true' facebook-id="invalidfacebookusername" src="images/test.png" />
            </div>
            <div style = {{display: 'table-cell',verticalAlign: 'middle',paddingLeft: '10%'}}>
              <a  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow : 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '10px',color: '#666666',letterSpacing: '0'}} > {people.username} </a>
              <div  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow: 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '10px',color: '#666666',letterSpacing: '0'}} >{people.age}, {people.occupation}</div>
            </div>
          </div>
        );
      },this.state.followers)}
      </div>
    );
  }
}

class FollowingList extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      following: []
    }
  }

  componentDidMount() {
    console.log("people mounted",this.props)
    if(this.props.following) {
      this.state.following = this.props.following
    }
    this.setState(this.state);
  }

  componentWillReceiveProps(newProps) {
    console.log("people received props",this.props)

    if(this.props!=newProps) {
      console.log("people received props different")
      this.props = newProps;
      if(this.props.following) {
        this.state.following = this.props.following
      }
      this.setState(this.state);
    }
  }

  render() {
    console.log("this people ",this.state);
    return(
      <div style={{display: 'flex',width:'100%',overflowY:'auto'}}>
      {R.map((people) => {
        return(
          <div onClick= {() => dispatchNavigationEvent(`profile/${people.id}/data`)} style={{cursor:'pointer',minHeight: '50px', height: '8.15%',display: 'table'}}>
            <div style={{display: 'table-cell',verticalAlign: 'middle'}}>
              <Avatar size="30" round='true' facebook-id="invalidfacebookusername" src="images/test.png" />
            </div>
            <div style = {{display: 'table-cell',verticalAlign: 'middle',paddingLeft: '10%'}}>
              <a  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow : 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '10px',color: '#666666',letterSpacing: '0'}} > {people.username} </a>
              <div  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow: 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '10px',color: '#666666',letterSpacing: '0'}} >{people.age}, {people.occupation}</div>
            </div>
          </div>
        );
      },this.state.following)}
      </div>
    );
  }
}

export class MainDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state = {
      self: false,
      followers: [],
      posts: [],
      following: [],
      isFollowing: null,
      current_mail: null,
      current_id: null,  // this is the user id
      age: null,
      email_id: null,
      profile_id: null,
      location: null,
      occupation : null,
      username :null,
      description: null,
      slideIndex: 0,
      isAjaxInProgress: false
    }
  }

  handleChange = (value) => {
  this.state.slideIndex = value;
  this.setState(this.state);
};

  initiateProfile = () => {
    console.log("initiated profile",this.props);
    this.state.isAjaxInProgress = false;
    let id = sessionStorage.getItem("app_id");
    let accesskey = sessionStorage.getItem("accessKey")
    let email = sessionStorage.getItem("email")
    if(!(email && id  && accesskey)) {
      console.log("no access keys here...")
      dispatchNavigationEvent(`/login`)
    }
    else {
      let ciphertext = CryptoJS.AES.decrypt(sessionStorage.getItem("accessKey"),sessionStorage.getItem("email")).toString(CryptoJS.enc.Utf8);
      let plain = ciphertext.toString(CryptoJS.enc.Utf8)
      console.log("id and ciphertext",id,ciphertext,plain)
      if(id == plain) {
        console.log("proper id")
        this.state.current_id = +id;
        this.state.current_mail = email;
        if(this.props.context.profile) {
          console.log("has profile...");
          this.state.profile_id = parseInt(this.props.context.profile.id);

          if(this.state.profile_id == this.state.current_id) {
            this.state.self = true;
          } else {
            this.state.slideIndex = 0;
          }
          this.state = R.merge(this.state,this.props.context.profile);
          this.state.followers = this.props.context.followers;
          this.state.following = this.props.context.following;
          this.state.posts = this.props.context.posts;
          console.log("this state followers here ",this.state.followers,typeof this.state.profile_id, typeof this.state.followers,+this.state.current_id == +this.state.followers[3]);
          if(R.any((element) => { console.log("element now is ",element); return element.id == this.state.current_id},this.state.followers) ) {
            console.log("is following")
            this.state.isFollowing = true;
          } else {
            R.any((element) => {
              console.log("ele is ",element,typeof element,this.state.current_id == element.id);
              element == this.state.current_id;
            },this.state.followers);
            console.log("not following");
            this.state.isFollowing = false;
          }
        }
        console.log("initiated profile ",this.state)
        this.setState(this.state);
      } else {
        dispatchNavigationEvent(`/login`)
      }
    }

    return false  }

  componentDidMount() {
    //check if self
    console.log("this props id is",this.props)
    this.initiateProfile();
  }

  componentWillReceiveProps(newProps) {
    console.log("will receive called",this.props,newProps);
    if(!(this.props == newProps)) {
      this.props = newProps;
      console.log("received props not same...");
      this.initiateProfile();
    }
  }

  onAction = () => {
     console.log("on action state ",this.state)
    if(!this.state.self) {
      if(this.state.isFollowing) {
        this.state.isAjaxInProgress = true;
        this.setState(this.state);
        axios.post(`profile/${this.state.current_id.toString()}/${this.state.profile_id}/unfollow`).then((response) => {
          console.log("here... unfollowing",response)
          if(response.status == 200) {
            console.log("dispatching")
            dispatchNavigationEvent(`/profile/${this.state.profile_id}/data`);
          }
        }).catch((error) => {
          console.log("error unfollowing ");
        })
      } else {
        this.state.isAjaxInProgress = true;
        this.setState(this.state);
        axios.post(`profile/${this.state.current_id.toString()}/${this.state.profile_id}/follow`).then((response) => {
          console.log("here... following",response)
          if(response.status == 200) {
            console.log("dispatching")
            dispatchNavigationEvent(`/profile/${this.state.profile_id}/data`);
          }
        }).catch((error) => {
          console.log("error following ");
        })
      }
    }
  }
  render() {
    console.log("this state is",this.state)
    this.state.description = 'Dummy description here : Sometimes I feel as though there are two me\'s, one coasting directly on top of the other: the superficial me, who nods when she\'s supposed to nod and says what she\'s supposed to say, and some other, deeper part, the part that worries and dreams... Most of the time they move along in sync and I hardly notice the split, but sometimes it feels as though I\'m two whole different people and I could rip apart at any second.' ;
    return(
      <div className={ "ui basic segment enterframe" + (this.state.isAjaxInProgress ? 'loading' : '') } style={{display: 'inline-block',width: '63.87%',height: '100%'}}>
        <div style={{marginRight: '23.88%',width: '66.12%'}}>
          <div style= {{width: '100%',display: 'inline-block'}} >
            <div style={{padding: '5px 5px 5px 0px','max-width': '70%',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow: 'ellipsis',float: 'left',fontFamily: 'Avenir-Book', fontSize: '28px',color: '#666666',letterSpacing: '1.4px'}}> {this.state.username}</div>
            {
              do {
                if(!this.state.self) {
                  <div style={{float: 'right'}}>
                    <div style={{height: '100%'}}>
                    <MuiThemeProvider>
                    <RaisedButton style={{height: '100%',color: "#FFFFFF"}} onTouchTap={::this.onAction} labelColor='#FFFFFF' buttonStyle= {{outline: 'none',padding: '5px',fontFamily: 'Avenir-Book' ,cursor: 'pointer',fontSize: '14px'  ,background: '#56A4FF', border: '1px solid #56A4FF', borderRadius: '1px',color: '#FFFFFF',letterSpacing: '0.7px',width: '100%',height: '100%'}}  label={this.state.isFollowing ? "Unfollow" : "Follow"} />
                    </MuiThemeProvider>
                    </div>
                </div>
                }
              }
            }
          </div>
          {
            do {
                if(this.state.age) {
                    <div style={{width: '100%',marginTop: '10px'}}>
                        <div style={{textAlign: 'justify',fontFamily: 'Avenir-Book', fontSize: '16px',letterSpacing: '0.8px',color: '#999999'}} > <p style={{textAlign: 'justify'}}>{this.state.age + (this.state.occupation ? "," + this.state.occupation : "") }
                        </p></div>
                    </div>
                }
            }
          }
          { do {
              if(this.state.location) {
                <div style={{width: '100%',marginTop: '8px'}}>
                    <div style={{fontFamily: 'Avenir-Book', fontSize: '16px',letterSpacing: '0.8px',color: '#999999'}} > {this.state.location }
                    </div>
                </div>
              }
            }
          }
          { do {
              if(this.state.description) {
                <div style={{width: '100%',marginTop: '20px'}}>
                    <div style={{fontFamily: 'Avenir-Book', fontSize: '12px',color: '#999999'}} > {this.state.description }
                    </div>
                </div>
              }
            }
          }
          <div style={{display: 'flex',width: '100%',marginTop: '20px'}}>
           <FacebookShareButton picture={this.state.image_url ? this.state.image_url : "https://zencil.herokuapp.com/images/zencil.png"} description = {this.state.description} title={"Zencil - " + this.state.username}  url = "https://zencil.herokuapp.com" children=""> <img style={{cursor: 'pointer'}} src="images/3.png" />  </FacebookShareButton>
           <div style={{display: 'inline-block', width: '8px'}}> </div>
           <GooglePlusShareButton   url = "https://zencil.herokuapp.com" children=""> <img style={{cursor: 'pointer'}} src="images/1.png" />  </GooglePlusShareButton>
           <div style={{display: 'inline-block', width: '8px'}}> </div>å
           <TwitterShareButton  title={"Zencil - " + this.state.username} via="zencil" hashtags= { [this.state.username, "zencil"] }  url="https://zencil.herokuapp.com" children=""> <img style={{cursor: 'pointer'}} src="images/2.png" />  </TwitterShareButton>
          </div>
          <div style={{marginTop: '50px',marginRight: '-25%'}}>
          <MuiThemeProvider>
          { do
            {
              if(this.state.self) {
                <Tabs onChange={this.handleChange} inkBarStyle={{border: '2px solid #69A8FF'}} tabItemContainerStyle={{background: '#FFFFFF'}}
                  value={this.state.slideIndex} >
                  <Tab buttonStyle={{fontFamily: 'Avenir-Book' ,fontSize: '14px',color: '#56A4FF',letterSpacing: "0.7px"}} label="Posts" value={0} />
                  <Tab buttonStyle={{fontFamily: 'Avenir-Book' ,fontSize: '14px',color: '#56A4FF',letterSpacing: "0.7px"}} label="My Account" value={1} />
                  <Tab buttonStyle={{fontFamily: 'Avenir-Book' ,fontSize: '14px',color: '#56A4FF',letterSpacing: "0.7px"}} label="People" value={2} />
                  <Tab buttonStyle={{fontFamily: 'Avenir-Book' ,fontSize: '14px',color: '#56A4FF',letterSpacing: "0.7px"}} label="Credits" value={3} />
                </Tabs>
              } else {
                <Tabs onChange={this.handleChange} inkBarStyle={{border: '2px solid #69A8FF'}} tabItemContainerStyle={{background: '#FFFFFF'}}
                  value={this.state.slideIndex} >
                  <Tab buttonStyle={{fontFamily: 'Avenir-Book' ,fontSize: '14px',color: '#56A4FF',letterSpacing: "0.7px"}} label="Posts" value={0} />
                  <Tab buttonStyle={{fontFamily: 'Avenir-Book' ,fontSize: '14px',color: '#56A4FF',letterSpacing: "0.7px"}} label="Followers" value={4} />
                  <Tab buttonStyle={{fontFamily: 'Avenir-Book' ,fontSize: '14px',color: '#56A4FF',letterSpacing: "0.7px"}} label="Following" value={5} />
                </Tabs>
              }
            }
          }
        </MuiThemeProvider>
        <SwipeableViews
          index={this.state.slideIndex}
          onChangeIndex={this.handleChange}
        >
          <div style={{width: '100%'}}>
            <PostList posts={this.state.posts}/>
          </div>
          <div style={styles.slide}>
            <MyAccount profile={R.clone(this.state)} />
          </div>
          <div style={{width: '100%'}}>
            <PeopleList followers={this.state.followers} following={this.state.following}/>
          </div>
          <div style={styles.slide}>
            Coming Soon ......
          </div>
          <div style={{width: '100%'}}>
            <PeopleList followers={this.state.followers} />
          </div>
          <div style={{width: '100%'}}>
            <PeopleList following={this.state.following} />
          </div>
          </SwipeableViews>
          </div>
          </div>
      </div>
    );
  }
}
export default class Profile extends React.Component {
  constructor(props) {
    console.log("cons called",props);
    super(props);
    this.props = props;
    this.state = {
      followers: [],
      following: [],
      profile: null,
      posts: [],
      flag : false
    }
  }

    componentDidMount() {
      console.log("xxxx ",this.props.params)
      axios.get(`profile/${this.props.params.id}/data`).then((response) => {
        console.log("important  ",response)
        console.log("profile is ",response.data)
        this.state.profile = response.data.user;
        this.state.followers = response.data.followers;
        this.state.following = response.data.following;
        this.setState(this.state);
      }).catch((error)=> {
        console.log("error in getting")
        this.setState(this.state);
      });
    }

  componentWillReceiveProps(newProps) {
    console.log("wil receive response ",this.props == newProps,this.state);
    if(!(this.props == newProps)) {
      this.state.profile  = null;
      this.setState(this.state);
      axios.get(`profile/${newProps.params.id}/data`).then((response) => {
        console.log("important  ",response)
        console.log("profile is ",response.data)
        this.state.profile = response.data.user;
        this.state.followers = response.data.followers;
        this.state.following = response.data.following;
        this.setState(this.state);
      }).catch((error)=> {
        console.log("error in getting")
        this.setState(this.state);
      });
    }
  }
  render() {
    console.log("profile render ",this.state)
    if(this.state.profile) {
    return (
            <div style={{height: '100%',width:'100%',paddingTop: '3.15%',background:'#FFFFFF',display:'flex',overflow: 'auto'}}>
            <Leftbar profile={this.state.profile} followers={this.state.followers} following={this.state.following} posts={this.state.posts} />
            <MainDisplay context = {this.state} followingChange = {this.followingChange}/>
            </div>
          );
    } else {
      return (<div className="ui basic segment enterframe loading"></div>);
    }
  }

}

import React from 'react';
import axios from 'axios';
import Footer from './Footer';
import store from '../store/AppStore';
import R from 'ramda';

import dispatchNavigationEvent from '../RoutingManagement';
import { PasswordStrength } from '../components/Utils';

export default class ResetForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      resetProgress: false,
      status: 'verifyLink',
      linkError: false,
      linkStatement: "",
      key: null,
      strenthValue: 0,
      strenthData: "Minimum 6 Characters",
      btnStatus: true
    };
  }

  componentDidMount() {
    console.log("here.... redirecting");
    dispatchNavigationEvent('/')
    }

  validateLink = (key) => {
    axios.post('/auth/forgot/verify_link',{
      key: key
    }).then((response) =>{
      response =  response.data;
      if(response.status === "valid"){
        this.state = R.merge(this.state, {
          status: 'ok',
          user_id: response.user_id,
          key: response.key
        });

        this.setState(this.state);
      }
    }).catch((error) =>{
        if(error.response) {
        if(error.response.data.status === 'invalid') {
          this.state = R.merge(this.state, {
            status: 'linkError',
            linkStatement: "Your password reset link expired. please try again."
          });

          this.setState(this.state);
        } else if(error.response.data.status === 'key_not_found') {
          this.state = R.merge(this.state, {
            status: 'linkError',
            linkStatement: "Invalid password reset link. please try again."
          });

          this.setState(this.state);
        }
      }
    });
  }

  doReset = (ev) => {
    ev.nativeEvent.preventDefault();

    let password = this.refs.newPassword.value;
    let id = this.state.key;

    this.setState({
      resetProgress: true
    });

    axios.post('/auth/forgot/reset_password', {
      id: id,
      password: password
    }).then((response) =>{
      response = response.data;
      if(response.status === "success"){
        this.state = R.merge(this.state, {
          resetProgress: false,
          status: 'resetSuccess'
        });

        this.setState(this.state);
      }
    }).catch((error) =>{
      this.state = R.merge(this.state, {
        resetProgress: false,
        status: 'resetFailure'
      });

      this.setState(this.state);
    });
  }

  keyUpHandler(e) {
    let pass = this.refs.newPassword.value;
    let {status, score} = PasswordStrength.calculate(pass);

    let progressBar = $('#password-strength').progress();
    progressBar.progress('set progress', score || 0);

    this.state = R.merge(this.state, {strenthValue: score, strenthData: status});

    if(pass == this.refs.confirmPassword.value) {
      this.state.btnStatus = false;
    } else {
      this.state.btnStatus = true;
      this.state.strenthValue = 'Passwords do not match';
    }

    this.setState(this.state);
  }

  passwordChecker (e) {
    let pass1 = this.refs.newPassword.value;
    let pass2 = this.refs.confirmPassword.value;

    if(pass1.length >= 6){
      if (pass1 != pass2){
        this.setState(R.merge(this.state, {btnStatus: true, strenthData: 'Passwords do not match'}));
      } else {
        this.keyUpHandler();
        this.setState(R.merge(this.state, {btnStatus: false}));
      }
    }
  }

  goToLogin = () =>{
    dispatchNavigationEvent('/login');
  }

  render() {
    return (
      <div>
        <div className="ui top fixed menu" style={{'height': '60px', 'zIndex': '10000000'}}>
          <div className="item juspay-navbar-logo">
            <div className="image">
              <img width="140" height="32" src="images/juspay-logo.png" />
            </div>
          </div>
        </div>
        <div className="ui basic segment">
          <div className="three column ui grid">
            <div className="content column">&nbsp;</div>
            <div className="ui segment content column" style={{'top': '-50%', 'transform': 'translateY(50%)'}}>
              <div style={{
                fontWeight: 'normal',
                fontSize: '25px',
                textAlign: 'center'
              }} className="ui header">
                Password Reset
              </div>
              <div className="ui hidden divider" />
              <div className="description">
                {do {
                  if(this.state.status === 'linkError') {
                    <div className="ui warning message">
                      <div className="header">Link Expired</div>
                      {this.state.linkStatement}

                      Click <a onClick={this.goToLogin}>here</a> to go to login.
                    </div>
                  } else if(this.state.status == 'ok' || this.state.status === 'resetFailure'
                            || this.state.status === 'verifyLink') {
                    <form
                        id="login_form"
                        onSubmit={this.doReset}
                        className={"ui form" + (this.state.status === 'verifyLink' ? ' loading' : '')}>
                      <div className="ui fluid input field">
                        <input onKeyUp={::this.keyUpHandler} type="password" ref="newPassword" placeholder="New Password" />
                      </div>
                      <div className="ui fluid input field">
                        <input type="password" onKeyUp={::this.passwordChecker}
                               ref="confirmPassword" placeholder="Confirm Password" />
                      </div>
                      <div className="description">
                        <div className="ui tiny indicating progress" id="password-strength">
                          <div className="bar" />
                          <div className="label">{this.state.strenthData}</div>
                        </div>
                      </div>
                      <div className="ui hidden divider" />
                      {do {
                         if(this.state.status === 'resetFailure') {
                           <div className="ui visible error message">
                             <div className="header">Error</div>
                             Failed to reset password, please try again.
                           </div>
                         }
                       }}
                      <button
                          disabled = {this.state.btnStatus}
                          className={"ui fluid green button" + (this.state.resetProgress ? ' disabled loading' : '')}>
                        Reset
                      </button>
                    </form>
                  } else if(this.state.status === 'resetSuccess') {
                    <div>
                      <div className="ui success message">
                        Password reset completed successfully.
                      </div>
                      <a className="ui basic button" onClick={this.goToLogin}>Click Here</a> to login.
                    </div>
                  }
                }}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

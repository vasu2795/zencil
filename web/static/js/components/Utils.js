import CryptoJS from 'crypto-js'

/**
 * Format UTC format date with logged in user's timezone.
 * Expect "userTimestamp" in sessionStorage; If it is not there
 * default timezone to Asia/Kolkata
 *
 * @return {[type]} [description]
 */
export const isLoggedIn = () => {
  console.log("islogged in ",sessionStorage.getItem("email"),sessionStorage.getItem("accessKey"))
  let id = sessionStorage.getItem("app_id");
  let accesskey = sessionStorage.getItem("accessKey")
  let email = sessionStorage.getItem("email")
  if(!(email && id  && accesskey)) {
    return false
  }
  let ciphertext = CryptoJS.AES.decrypt(sessionStorage.getItem("accessKey"),sessionStorage.getItem("email")).toString(CryptoJS.enc.Utf8);
  let plain = ciphertext.toString(CryptoJS.enc.Utf8)
  console.log("id and ciphertext",id,ciphertext,plain)
  if(id == plain) {
    return true
  }
  return false
}

export const getCurrentId = () => {
  let id = sessionStorage.getItem("app_id");
  let accesskey = sessionStorage.getItem("accessKey")
  let email = sessionStorage.getItem("email")
  if(!(email && id  && accesskey)) {
    return -1
  }
  let ciphertext = CryptoJS.AES.decrypt(sessionStorage.getItem("accessKey"),sessionStorage.getItem("email")).toString(CryptoJS.enc.Utf8);
  let plain = ciphertext.toString(CryptoJS.enc.Utf8)
  console.log("id and ciphertext",id,ciphertext,plain)
  if(id == plain) {
    return id
  }
  return -1
}

// Given a password string, return as a percentage
// the strength of the password

import React from 'react';
import axios from 'axios';
import R from 'ramda';
import Avatar from 'react-avatar';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import {getCurrentId} from '../Utils'
import { hashHistory } from 'react-router';
import store from '../../store/AppStore';
import { teardownApp } from '../../actions/AuthActions';
import dispatchNavigationEvent from '../../RoutingManagement';
import socket from '../../Notifications';
import Center from 'react-center';
import theme from '../../../css/theme.css'
import Autosuggest from 'react-autosuggest';

// Imagine you have a list of languages that you'd like to autosuggest.
const languages = [
  {
    title: '1970s',
    languages: [
      {
        name: 'C',
        year: 1972
      }
    ]
  },
  {
    title: '1980s',
    languages: [
      {
        name: 'C++',
        year: 1983
      },
      {
        name: 'Perl',
        year: 1987
      }
    ]
  },
  {
    title: '1990s',
    languages: [
      {
        name: 'Haskell',
        year: 1990
      },
      {
        name: 'Python',
        year: 1991
      },
      {
        name: 'Java',
        year: 1995
      },
      {
        name: 'Javascript',
        year: 1995
      },
      {
        name: 'PHP',
        year: 1995
      },
      {
        name: 'Ruby',
        year: 1995
      }
    ]
  },
  {
    title: '2000s',
    languages: [
      {
        name: 'C#',
        year: 2000
      },
      {
        name: 'Scala',
        year: 2003
      },
      {
        name: 'Clojure',
        year: 2007
      },
      {
        name: 'Go',
        year: 2009
      }
    ]
  },
  {
    title: '2010s',
    languages: [
      {
        name: 'Elm',
        year: 2012
      }
    ]
  }
];

// Teach Autosuggest how to calculate suggestions for any given input value.

const getSuggestionValue = (suggestion) => {
  console.log("here in sugg value....");
  return suggestion.username;
}

const getSuggestions = value => {
  let promise = axios.post(`/profile`,{"criteria" : "all"})
  promise.then((response) => {
    console.log("prof resp is ",response.data)
  }).catch((error) => {
    console.log("error  is ",error)
  })
  return promise

};

const getSectionSuggestions = (section)  =>{
  return section.data;
}

// Use your imagination to render suggestions.
const renderSuggestion = (suggestion,{query}) => {
  console.log("query is ",suggestion,query)
  return(
    <div onClick= {() => dispatchNavigationEvent(`profile/${suggestion.id}/data`)} style={{cursor:'pointer',minHeight: '50px', height: '50px',display: 'table'}}>
      <div style={{display: 'table-cell',verticalAlign: 'middle'}}>
        <Avatar size="30" round='true' facebook-id="invalidfacebookusername" src="images/test.png" />
      </div>
      <div style = {{display: 'table-cell',verticalAlign: 'middle',paddingLeft: '20px',paddingRight: '20px'}}>
        <a  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow : 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '14px',color: '#666666',letterSpacing: '0'}} > {suggestion.username} </a>
        <div  style={{display:'table-row',verticalAlign: 'middle',whiteSpace: 'nowrap',overflow: 'hidden',textOverflow: 'ellipsis',fontFamily: 'Avenir-Roman',fontSize: '12px',color: '#666666',letterSpacing: '0'}} >{suggestion.age ? (suggestion.age + (suggestion.occupation ? ("," + suggestion.occupation) : "")) : (suggestion.occupation ? suggestion.occupation : (suggestion.location ? suggestion.location : suggestion.email_id)) }</div>
      </div>
    </div>

  );
}

const alwaysRenderSuggestions = () => {
    return true;
}

const escapeRegexCharacters = (str) => {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

class Example extends React.Component {
  constructor() {
    super();

    // Autosuggest is a controlled component.
    // This means that you need to provide an input value
    // and an onChange handler that updates this value (see below).
    // Suggestions also need to be provided to the Autosuggest,
    // and they are initially empty because the Autosuggest is closed.
    this.state = {
      value: '',
      suggestions: []
    };
  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };


  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }) => {
    const inputValue = value.trim().toLowerCase();
    const inputLength = inputValue.length;

    let promise = getSuggestions(value)
    promise.then((response) => {
      const escapedValue = escapeRegexCharacters(value.trim());

      if (escapedValue === '') {
        return [];
      }

      const regex = new RegExp('^' + escapedValue, 'i');
      console.log("resposne data is ",response.data)
      let result =  response.data
        .map(section => {
          console.log("is section ",section,section.data)
          return {
            title: section.title,
            data: section.data.filter(data => regex.test(data.username))
          };
        })
        .filter(section => section.data.length > 0);
      console.log("get results ",response.data);
      console.log("suggestions is ",result);
      this.state.suggestions = (inputLength === 0 ? [] : result);
      this.setState(this.state);
    }).catch((error) => {
      console.log("error is ",error);
    })
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  renderSectionTitle = (section) => {
    console.log("render section title is ",section);
    return(
      <strong>{section.title}</strong>
    );
  }
  render() {
    const { value, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input element.
    const inputProps = {
      placeholder: 'Search',
      value,
      onChange: this.onChange
    };

    var theme = {
    container: 'react-autosuggest__container',
    containerOpen: 'react-autosuggest__container--open',
    input: 'react-autosuggest__input',
    suggestionsContainer: 'react-autosuggest__suggestions-container',
    suggestion: 'react-autosuggest__suggestion',
    suggestionFocused: 'react-autosuggest__suggestion--focused',
    sectionContainer: 'react-autosuggest__section-container',
    sectionTitle: 'react-autosuggest__section-title',
    sectionSuggestionsContainer: 'react-autosuggest__section-suggestions-container'
};

    // Finally, render it!
    return (
      <Autosuggest theme={theme}
        multiSection={true}
        alwaysRenderSuggestions	= {true}
        getSuggestionValue={getSuggestionValue}
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        renderSuggestion={renderSuggestion}
        renderSectionTitle={this.renderSectionTitle}
        inputProps={inputProps}
        getSectionSuggestions = {getSectionSuggestions}
      />
    );
  }
}


 const MediumText = (props) => (
     <div style={{'margin-top': '20px', 'margin-bottom': '20px','height':'20px'}} ><a onClick= {() => props.onSelect(props.text)} style={{'font-family': ' Avenir-Book','cursor': 'pointer','fontSize': '16px','opacity':  '1','color':  props.color,'textAlign': 'left','letterSpacing': '0.8px'}}>{props.text}</a></div>
 );

 export default class Navbar extends React.Component {
    constructor(props) {
      super(props);
      this.props = props;
      this.state = {
        sortBy: "Latest"
      }
    }

    componentDidMount() {
      console.log("here.... in navbar")
      this.setState(this.state);
    }



    logout() {
      axios.get('/auth/logout').then((response) => {
        if(response.data.status === 'logged_out') {
          sessionStorage.clear();
          store.dispatch(teardownApp());
          hashHistory.push('/login')
        }
      }).catch((err) => {
        console.error(err);
      });
    }

    handleChange = (ev,value) => {
      console.log("handling change ",value,ev);
      if(value == "1") {
        console.log("current id is ",getCurrentId());
        let id = getCurrentId();
        if(id != -1) {
          dispatchNavigationEvent(`/profile/${getCurrentId()}/data`);
        } else {
          dispatchNavigationEvent('/login')
        }
      } else {
        sessionStorage.clear();
        store.dispatch(teardownApp());
        hashHistory.push('/login')
      }
    }

    render() {
        const imageUrl = "./zencil.png"

        let onSelect = (sortBy) => {
          console.log("onselect sortby is ",sortBy)
          this.state.sortBy = sortBy;
          this.setState(this.state);
          this.props.onSelect(sortBy);
        }

        return (
          <div id="topBar" className="ui top fixed menu" style={{'position':'relative','min-width': '1024px','height': '60px'}}>
            <div style={{'width' : '20%', 'height': '100%','float':'left','display': 'table'}}>
              <div onClick={() => {dispatchNavigationEvent('/')}} style={{'cursor': 'pointer','display': 'table-cell','verticalAlign': 'middle','textAlign':'center','margin-left':'7.81%','margin-top':'20px','margin-bottom':'20px'}}>
                <img src="images/zencil.png"/>
              </div>
            </div>
            <div style={{'text-align':'center','position':'relative','height': '100%'}} className="vr">&nbsp;</div>
            <div style={{'text-align':'center','float':'right','height': '100%', 'width': '80%'}}>
              <div style = {{'width': '28.21%','height': '100%','float':'left', 'margin-left': '3.75%','display': 'inline-block'}} >
                <div style={{'float':'left','height':'100%'}}><MediumText  color={this.state.sortBy == "Latest" ? "#1e70bf" : "#999999"} onSelect={(sortBy) => onSelect(sortBy)} text="Latest" /></div>
                <div style={{'float':'right','height':'100%'}}><MediumText  color={this.state.sortBy == "A-Z" ? "#1e70bf" : "#999999"} onSelect={(sortBy) => onSelect(sortBy)} text="A-Z" /></div>
                <div style={{'margin-left': 'auto','margin-right': 'auto','text-align':'center','height':'100%','width':'35%'}}><MediumText  color={this.state.sortBy == "Following" ? "#1e70bf" : "#999999"} onSelect={(sortBy) => onSelect(sortBy)} text="Following" /></div>
              </div>
              <div style={{'float': 'left','width': '60%'}}>
                <div style={{'float':'left','height':'100%','width': '50%'}}>
                  <div onClick={() => { dispatchNavigationEvent(`post/3/view`)}} style={{cursor: 'pointer','float':'right','height':'100%'}}>
                    <MediumText text="Write a story"/>
                  </div>
                  <div style={{'float':'right','height':'100%','margin-right': '10px','margin-top': '24px','margin-left': '16px'}}>
                    <img  src="images/hearts.png"/>
                  </div>
                </div>
                <div style={{'float':'left','width':'50%','height':'100%'}}>
                          <div style={{'float':'left','width':'100%','height':'100%'}}>
                    <Example />
                    <div style={{'float':'left','marginTop': '10px','width':'16%','marginBottom': '10px','height':'40px','background': '#D8D8D8','border-radius': '0px 3px 3px 0px'}}>
                      <img style={{'margin-bottom': '10px', 'margin-top': '10px'}} src="images/search.png" />
                    </div>
                </div>
                </div>
              </div>
              <div style={{'background': '#FFFFFF','float': 'right','height':'100%','width': '8%'}}>
                <MuiThemeProvider>
                <IconMenu iconButtonElement={<IconButton>  <div  className="round" style={{'float': 'right','margin-top':'10px','margin-left': '10px','width':'40px','height':'40px'}}>
                    <Avatar round = 'true' size="40" facebook-id="invalidfacebookusername" src="images/test.png" /> </div></IconButton>}
                    anchorOrigin={{horizontal: 'right', vertical: 'bottom'}} onChange={::this.handleChange}
                    targetOrigin={{horizontal: 'right', vertical: 'bottom'}}
                    >
                    <MenuItem value="1" primaryText="My Profile" />
                    <MenuItem value="2" primaryText="Logout" />
                    </IconMenu>
                  </MuiThemeProvider>
              </div>
            </div>
         </div>);
    }
}

import React from 'react';
import R from 'ramda';

export default class DataTable extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;

    this.state = {
      data: {
        list: [],
        total: null,
        count: null,
        offset: null
      },
      page: 1,
      itemsPerPage: 20,
      totalItems: null
    };
  }

  loadItemsForPage() {
    let start = this.state.page * this.state.itemsPerPage;

    let itemsPromise = this.props.loadItems(0, start);
    itemsPromise.then((response) => {
      console.log(response.data);
      this.state = response.data;
    }).catch((error) => {
      console.log(error);
    });
  }

  componentDidMount() {
    this.loadItemsForPage();
  }

  render() {
    console.log(this.props);

    return (
      <div className="ui basic segment">
        <table className="ui basic striped table">
          <thead>
            <tr>
              {R.map((field) => {
                return (
                  <th>{field.pretty_name}</th>
                )
              }, this.props.fields)}
            </tr>
          </thead>
          <tbody>
          {R.map((item) => {
            return (
              <tr>
                {R.map((field) => {
                  return (
                    <td>{item[field.name]}</td>
                  )
                }, this.props.fields)}
              </tr>
            )
          }, this.state.data.list)}
          </tbody>
        </table>
      </div>
    );
  }
}

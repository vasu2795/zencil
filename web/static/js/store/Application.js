import R from 'ramda';

/*
	App status reducer manages global app status.
*/
export function appStatus(state={}, action) {
  switch(action.type) {
    case 'INIT':
      let isLoggedIn = sessionStorage.getItem('loginStatus') === 'logged_in';

      return Object.assign({}, state, {
        appStatus: isLoggedIn ? 'STARTED' : 'REQUIRE_AUTH'
      });

    case 'TEARDOWN':
      return Object.assign({}, state, {
        appStatus: 'REQUIRE_AUTH'
      });

    default:
      return state;
  }
}

export function merchantList(state={}, action) {
  switch(action.type) {
    case 'ADD_MERCHANTS':
      return R.reduce((agg, item) => {
        agg[item.merchant_id] = item;
        return agg;
      }, state, action.merchants);

    case 'DELETE_MERCHANTS':
      state.merchants = R.filter((merchant) => {
        return merchant.merchant_id === action.merchantId;
      }, state.merchants);

      return Object.assign({}, state);
    default:
      return state;
  }
}

export function search(state={}, action) {
  switch (action.type) {
    case 'ADD_DATA':

  }
}

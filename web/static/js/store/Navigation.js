export default function navigateTo(state={}, action) {
  switch(action.type) {
    case 'NAVIGATION_ACTION':
      console.log("action to is",action.to)
      return Object.assign({}, state, {
        lastVisited: action.to
      });
    default:
      return state;
  }
}

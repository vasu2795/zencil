defmodule Zencil.PostView do
  use Zencil.Web, :view

  def render("list_posts.json", %{ posts: posts }) do
    posts
  end

  def render("list_pending_posts.json", %{ posts: posts }) do
    posts
  end

  def render("list_deleted_posts.json", %{ posts: posts }) do
    posts
  end

  def render("list_discarded_posts.json", %{ posts: posts }) do
    posts
  end

  def render("get_posts.json", %{ posts: posts }) do
    posts
  end
end

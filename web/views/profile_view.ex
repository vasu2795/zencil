defmodule Zencil.ProfileView do
  use Zencil.Web, :view

  def render("get_profiles.json", %{ profiles: profiles }) do
    profiles
  end

end

var CopyWebpackPlugin = require("copy-webpack-plugin");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var path = require('path');
var webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
  entry: [__dirname + "/web/static/js/app.js"],
  output: {
    path: __dirname + "/priv/static/js",
    filename: "app.js"
  },
  resolve: {
    modules: [ "node_modules", __dirname + "/web/static/js" ],
    extensions: ['.js', '.jsx'],
    alias: {
      'react' : path.resolve(__dirname, 'node_modules/react')
      }
  },
  module: {
    devtool: "source-map",
    loaders: [
      {
        test: /\.json$/,
        loader: "json-loader"
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          "presets": ["es2015","stage-0","react"]
        }
      },
      {
        test: /\.css$/,
        loader: "css-loader"
      },
      {
        test: /\.png$/,
        loader: "url-loader"
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: "file"
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin("css/app.css"),
    new CopyWebpackPlugin([{ from: "./web/static/assets" }]),
    new webpack.DefinePlugin({
  'process.env':{
    'NODE_ENV': JSON.stringify('development')
  }
}),
new UglifyJSPlugin({sourceMap: true})

  ]
}
